//
//  Example3.swift
//  TakiKids
//
//  Created by hiba on 29/11/2022.
//

import SwiftUI

struct DragAndDropTwoBoxsView: View {

    @StateObject private var VM = DragAndDropTwoBoxsViewModel()
    
    private let columns = [
        GridItem(.adaptive( minimum: 71 )),
        GridItem(.adaptive( minimum: 71 ))
    ]
    
    var body: some View {
        ZStack{
            //Background Image
            Image("B")
                .resizable()
                .ignoresSafeArea()
            
            VStack(spacing: 10){
                Header
                
                Boxes
                
                OptionsView
            }
        }
        .onDrop(of: [.url], isTargeted: .constant(false)){ providers in
            if let first =  providers.first{
                let _ = first.loadObject(ofClass: URL.self) { value, error in
                    guard let url =  value else{return}
                    OnDrop(dragList: $VM.LeftList, dropList: $VM.ChoicesList, url: "\(url)")
                    OnDrop(dragList: $VM.RightList, dropList: $VM.ChoicesList, url: "\(url)")
                }
            }
            return false
        }
    }
}

extension DragAndDropTwoBoxsView {
    private var Header: some View {
        HStack{
            Image("IDONTHEAR")
            Spacer()
            Image("LetterI")
            Spacer()
            Image("IHEAR")
        }
        .padding(.horizontal, 25)
        .padding(.top,5)
    }
    
    private var Boxes: some View {
        HStack(spacing: 50){
            ContainerView(actionAnimation: $VM.animateLeftBox, firstList: $VM.LeftList, secondList: $VM.RightList, choicesList: $VM.ChoicesList)
            ContainerView(actionAnimation: $VM.animateRightBox, firstList: $VM.RightList, secondList: $VM.LeftList, choicesList: $VM.ChoicesList)
        }
    }
    
    private var OptionsView: some View {
        HStack{
            Spacer()
            LazyHGrid(rows: columns){
                ForEach(VM.ChoicesList, id: \.self) {item in
                    ZStack{
                        ZStack{
                            Image(item.state == ModelState.empty ? "" : "\(item.image)")
                            
                        }
                        .drag(if: item.image != "" ){
                            return .init(contentsOf: URL(string: "\(item.id)"))!
                        }
                    }
                }
                
            }
            Spacer()
            Button {
                CheckWinState(leftList:  $VM.LeftList, rightList: $VM.RightList, choicesList: $VM.ChoicesList, correctList: VM.correctList, animateLeftBox: $VM.animateLeftBox, animateRightBox: $VM.animateRightBox)
            } label: {
                Image("check_button")
                
            }
            .offset(y:40)
        }
    }
}

struct Example3_Previews: PreviewProvider {
    static var previews: some View {
        DragAndDropTwoBoxsView()
    }
}
