//
//  DragAndDrop2Boxs.swift
//  TakiKids
//
//  Created by hiba on 30/11/2022.
//

import Foundation
import SwiftUI

///Drop element to a container
func OnDrop(dragList: Binding<[Model]>, dropList: Binding<[Model]>, url: String) {
    if let itemIndex = dragList.firstIndex(where: { $0.id == Int("\(url)") }) {
        dropList.wrappedValue.append(dragList[itemIndex].wrappedValue)
        dragList.wrappedValue.remove(at: itemIndex)
    }
}
func CheckWinState(leftList: Binding<[Model]>, rightList: Binding<[Model]>, choicesList: Binding<[Model]>, correctList: [[Int]] , animateLeftBox :Binding<Bool> ,animateRightBox:Binding<Bool> ) {
    if correctList[0].count != leftList.count{
        animateLeftBox.wrappedValue = true
    }
    if correctList[1].count != rightList.count{
       animateRightBox.wrappedValue = true
    }
    
    for index in leftList.indices {
        let item = leftList[index]
        print(item.id , "l")
        if(!correctList[0].contains(item.id)){
           animateLeftBox.wrappedValue = true
        }
    }
    
    for index in rightList.indices {
        let item = rightList[index]
        print(item.id , "r")
        if(!correctList[1].contains(item.id)){
            animateRightBox.wrappedValue = true
        }
    }
}
