//
//  BoxView.swift
//  TakiKids
//
//  Created by hiba on 30/11/2022.
//  Refactored by Ahlem :D
//

import SwiftUI

struct ContainerView: View {
    @Binding var actionAnimation: Bool
    var firstList: Binding<[Model]>
    var secondList: Binding<[Model]>
    var choicesList: Binding<[Model]>
    
    @State var animationDegree  = 360

    let columns = [
        GridItem(.flexible(), alignment: .top),
        GridItem(.flexible(), alignment: .top),
        GridItem(.flexible(), alignment: .top),
        GridItem(.flexible(), alignment: .top)
    ]
    
    var width = UIScreen.main.bounds.width * 0.45
    var height = UIScreen.main.bounds.height * 0.4
    
    var body: some View {
        ZStack(){
            BoxView
            ItemsGrid
        }
        .onDrop(of: [.url], isTargeted: .constant(false)) { onDrop(providers: $0) }
    }
}

extension ContainerView {
    private var BoxView: some View {
        Image("state_1TwoBoxs")
            .resizable()
            .frame(width: width , height: height)
            .rotationEffect(.degrees(Double(animationDegree)))
            .animation(Animation.linear.repeatCount(Int(3)).speed(5), value: animationDegree)
            .onChange(of: actionAnimation, perform: { _ in
                if actionAnimation {
                    animationDegree = 358
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        actionAnimation = false
                        animationDegree = 360
                    }
                }
            })
    }
    
    private var ItemsGrid: some View {
        LazyVGrid(columns: columns, spacing: 5) {
            ForEach(firstList, id: \.self){ item in
                ZStack{
                    Image(item.state.wrappedValue == ModelState.empty ? "" : "\(item.image.wrappedValue)")
                        .resizable()
                        .rotationEffect(.degrees(Double(animationDegree)))
                        .animation(Animation.linear.repeatCount(Int(3)).speed(5), value: animationDegree)
                        .onChange(of: actionAnimation, perform: { _ in
                            if actionAnimation {
                                animationDegree = 358
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    actionAnimation = false
                                    animationDegree = 360
                                }
                            }
                        })
                }
                .onTapGesture(count: 2) {
                    //On Double click return the item to choices list
                    if item.state.wrappedValue != ModelState.correct
                    {
                        OnDrop(dragList: firstList, dropList: choicesList, url: "\(item.id)")
                    }
                }
                .drag(if: item.state.wrappedValue != ModelState.correct) {return .init(contentsOf: URL(string: "\(item.id)"))!}
            }
        }
        .padding(10)
        .frame(width: width , height: height, alignment: .topLeading)
    }
    
    private func onDrop(providers: [NSItemProvider]) -> Bool {
        if let first =  providers.first{
            let _ = first.loadObject(ofClass: URL.self) { value, error in
                guard let url =  value else{ return }
                
                if choicesList.wrappedValue.contains(where: { "\($0.id)" == url.relativeString }) {
                    //When the item is being dragged from the choices list
                    OnDrop(dragList: choicesList, dropList: firstList, url: "\(url)")
                } else {
                    //When the item is being dragged from a container
                    OnDrop(dragList: secondList, dropList: firstList, url: "\(url)")
                }
            }
        }
        return false
    }
}
