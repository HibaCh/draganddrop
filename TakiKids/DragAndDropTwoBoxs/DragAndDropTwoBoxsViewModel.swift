//
//  DragAndDropTwoBoxsModelView.swift
//  TakiKids
//
//  Created by hiba on 6/12/2022.
//

import Foundation
import SwiftUI

class DragAndDropTwoBoxsViewModel : ObservableObject {
    
    @Published var LeftList: [Model] = [
    ]

    @Published var RightList: [Model] = [ ]

    @Published var ChoicesList: [Model] = [
        Model(id: 1 , image: "1", state: ModelState.normal ),
        Model(id: 2 , image: "2"  , state: ModelState.normal ),
        Model(id: 3 , image: "3" , state: ModelState.normal ),
        Model(id: 4 , image: "4"  , state: ModelState.normal ),
        Model(id: 5 , image: "5" , state: ModelState.normal ),
        Model(id: 6 , image: "6"  , state: ModelState.normal ),
        Model(id: 7 , image: "7"  , state: ModelState.normal ),
        Model(id: 8 , image: "8" , state: ModelState.normal )
    ].shuffled()

    @Published var animateLeftBox: Bool = false
    @Published var animateRightBox: Bool = false

    var correctList: [[Int]] = [[1, 3, 4, 5 ,8 ], [2, 6, 7]]

}
