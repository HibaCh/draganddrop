//
//  View.swift
//  TakiKids
//
//  Created by hiba on 15/12/2022.
//

import Foundation
import SwiftUI

//TextFildUI
@ViewBuilder
func eyeFild(isSecureField : Binding<Bool>, fildName : Binding<String> , LocalizedFildName : String  )->some View{
  VStack{
      if isSecureField.wrappedValue {
            SecureField( "" , text: fildName)
                .modifier(StyleOfButton(showPlaceHolder: fildName.wrappedValue.isEmpty, placeholder:LocalizedStringKey(LocalizedFildName)))
        } else {
            TextField( "" , text: fildName)
                .modifier(StyleOfButton(showPlaceHolder: fildName.wrappedValue.isEmpty, placeholder:LocalizedStringKey(LocalizedFildName)))
        }
    }
  .overlay(Image(systemName: isSecureField.wrappedValue ? "eye.slash" : "eye")
        .padding(.trailing , 25)
        .foregroundColor(.blue)
        .onTapGesture {
            isSecureField.wrappedValue.toggle()
        } , alignment: .trailing
    )
}



struct StyleOfButton: ViewModifier {
    var showPlaceHolder: Bool
    var placeholder: LocalizedStringKey
    var colorText : Color = .black
    var corner = 50
    var width = UIScreen.main.bounds.width * 0.35
    func body(content: Content) -> some View {
        HStack{
            ZStack(alignment: .leading) {
                if showPlaceHolder {
                    Text(placeholder)
                        .foregroundColor(Color.blue)
                    
                }
                content
                    .foregroundColor(colorText)
                    .padding(5.0)
            }
        }
        .padding(10)
        .background(
            Rectangle()
                .foregroundColor(.white)
                .cornerRadius(CGFloat(corner))
        )
        .frame(width: width)
    }
}
