//
//  DragAndDropFunc.swift
//  TakiKids
//
//  Created by hiba on 29/11/2022.
//

import Foundation
import SwiftUI

func onDrop_DropArea(providers: [NSItemProvider], item: Model, dropList: Binding<[Model]>, choicesList: Binding<[Model]>) -> Bool {
    if let first =  providers.first{
        let _ = first.loadObject(ofClass: URL.self) { value, error in
            guard let url =  value else{ return }
            DropFunc(firstList: choicesList, secondList: dropList, item: item, url:"\(url)")
            DropFunc(firstList: dropList, secondList: dropList, item: item, url:"\(url)")
        }
    }
    return false
}

func onDrop_DragArea(providers: [NSItemProvider], item: Model, dropList: Binding<[Model]>, choicesList: Binding<[Model]>) -> Bool {
    if let first =  providers.first{
        let _ = first.loadObject(ofClass: URL.self) { value, error in
            guard let url =  value else{return}
            DropFunc(firstList: dropList, secondList: choicesList, item: item, url:"\(url)")
        }
    }
    return false
}

///drop and drag  the item of the dropList  in the choice place
@ViewBuilder
func DropArea( content: some View, item: Model, dropList: Binding<[Model]>, choicesList:Binding<[Model]>)->some View {
    HStack(spacing:12) {
        content
            .onDrop(of: [.url], isTargeted: .constant(false)){onDrop_DropArea(providers: $0, item: item, dropList: dropList, choicesList: choicesList)}
            .drag(if: (item.image != "" && item.state != ModelState.correct)) {
                return .init(contentsOf: URL(string: "\(item.id)"))!
            }
            .onTapGesture(count: 2) {
                if (item.image != "" && item.state != ModelState.correct) {
                    OnClick(firstList: choicesList, secondList: dropList, item: item)
                }
            }
    }
}

///drop and drag  the item of the choicesList  in the choice place
@ViewBuilder
func DragArea( content: some View, item :Model, dropList: Binding<[Model]>, choicesList: Binding<[Model]> , isClickable : Bool = false)->some View {
    HStack(spacing:12) {
        content
            .onDrop(of: [.url], isTargeted: .constant(false)){onDrop_DragArea(providers: $0, item: item, dropList: dropList, choicesList: choicesList)}
            .drag(if: item.image != "" ) {
                return .init(contentsOf: URL(string: "\(item.id)"))!
            }
            .onTapGesture(count: 2) {
                if (item.image != "" && !isClickable) {
                    OnClick(firstList: dropList, secondList: choicesList, item: item)
                }
            }
    }
}

///Drop element to a container
func DropFunc(firstList: Binding<[Model]>, secondList: Binding<[Model]>, item: Model, url: String){
    if item.state != ModelState.correct {
        if let TopToBottomIndex = firstList.firstIndex( where: { $0.id == Int("\(url)") } ) {
            let aux = item
            let currentItemIndex = secondList.firstIndex(where: {$0.id == item.id})!
            
            secondList[currentItemIndex].image.wrappedValue = firstList[TopToBottomIndex].image.wrappedValue
            firstList[TopToBottomIndex].image.wrappedValue = aux.image
            
            secondList[currentItemIndex].state.wrappedValue = ModelState.normal
            if firstList[TopToBottomIndex].image.wrappedValue != "" {
                firstList[TopToBottomIndex].state.wrappedValue =  ModelState.normal
            }else{
                firstList[TopToBottomIndex].state.wrappedValue = aux.state
            }
            
            secondList[currentItemIndex].linkedId.wrappedValue = firstList[TopToBottomIndex].linkedId.wrappedValue
            firstList[TopToBottomIndex].linkedId.wrappedValue = aux.linkedId

        }
    }
}

///On Double click the item draging in the  container
func OnClick(firstList: Binding<[Model]>, secondList: Binding<[Model]>, item: Model){
    if let  emptyIndex = firstList.firstIndex( where: { $0.image.wrappedValue == "" } ) {
        let aux =  firstList[emptyIndex].wrappedValue
        let currentItemIndex = secondList.firstIndex(where: {$0.id == item.id})!
        
        firstList[emptyIndex].image.wrappedValue = secondList[currentItemIndex].image.wrappedValue
        secondList[currentItemIndex].image.wrappedValue = aux.image
        
        firstList[emptyIndex].state.wrappedValue = ModelState.normal
        secondList[currentItemIndex].state.wrappedValue = aux.state
        
        firstList[emptyIndex].linkedId.wrappedValue = secondList[currentItemIndex].linkedId.wrappedValue
        secondList[currentItemIndex].linkedId.wrappedValue = aux.linkedId
        
    }
}


func checkWinStateDragAndDrop(dropList: Binding<[Model]>, correctList: [[Int]], actionAnimation:Binding<Bool> , answersList : Binding<[[Int]]>) {
    for index in dropList.indices {
        let item = dropList[index]
        if item.image.wrappedValue != "" && item.state.wrappedValue != ModelState.correct {
            dropList[index].state.wrappedValue = ModelState.wrong
            if correctList.contains([item.id, item.linkedId.wrappedValue]) || correctList.contains([item.linkedId.wrappedValue, item.id]) {
                dropList[index].state.wrappedValue = ModelState.correct
                answersList.wrappedValue.append([item.linkedId.wrappedValue, item.id])
            }
        }
      
    }
    if answersList.wrappedValue.sorted(by: {$0[0] < $1[0] }) == correctList.sorted(by: {$0[0] < $1[0] }){
        SoundManger.instance.playSound(sound: .tada)
    }else{
        actionAnimation.wrappedValue = true
    }

}
