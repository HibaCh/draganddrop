//
//  DragandDrop.swift
//  TakiKids
//
//  Created by hiba on 6/12/2022.
//

import Foundation
import SwiftUI

class DragAndDropViewModel: ObservableObject {
    @Published var dropList: [Model] = []
    @Published var choicesList: [Model] = []
    @Published var actionAnimation : Bool = false
    @Published var correctList: [[Int]] = []
    
    @Published var answersList: [[Int]] = []
    @Published var images: [String] = []
    @Published var states: [String] = []
    
    init(type: Views){
        switch (type)  {
        case .first:
            dragAndDropView1()
        case .second:
            dragAndDropView2()
        case .third:
            dragAndDropView3()
        }
    }
    
    func dragAndDropView1() {
        self.dropList = [
            Model(id: 1 , image: ""  , state: ModelState.empty , linkedId: 1),
            Model(id: 2 , image: ""  , state: ModelState.empty , linkedId: 2),
            Model(id: 3 , image: ""  , state: ModelState.empty , linkedId: 3),
            Model(id: 4 , image: ""  , state: ModelState.empty , linkedId: 4)
        ]
        self.choicesList = [
            Model(id: 5 , image: "choice_1" , state: ModelState.normal , linkedId: 5),
            Model(id: 6 , image: "choice_2"  , state: ModelState.normal , linkedId: 6),
            Model(id: 7 , image: "choice_3", state: ModelState.normal , linkedId: 7),
            Model(id: 8 , image: "choice_4"  , state: ModelState.normal , linkedId: 8)
        ].shuffled()
        
        self.images = ["img1", "img2", "img3","img4"]
        self.states = ["boutton", "boutton_true", "boutton_false"]
        self.correctList = [[1, 5], [2, 8], [3, 6], [4, 7]]
    }
    
    func dragAndDropView2() {
        self.dropList = [
            Model(id: 1 , image: "P"  , state: ModelState.correct , linkedId: 1),
            Model(id: 2 , image: ""  , state: ModelState.empty , linkedId: 2),
            Model(id: 3 , image: "A"  , state: ModelState.correct , linkedId: 3),
            Model(id: 4 , image: "R"  , state: ModelState.correct , linkedId: 4)
        ]
        self.choicesList = [
            Model(id: 5 , image: "A"  , state: ModelState.normal , linkedId: 5),
            Model(id: 6 , image: "I"  , state: ModelState.normal , linkedId: 6),
            Model(id: 7 , image: "I"  , state: ModelState.normal , linkedId: 7),
            Model(id: 8 , image: "U" , state: ModelState.normal , linkedId: 8),
            Model(id: 9 , image: "E" , state: ModelState.normal , linkedId: 9)
        ]
        self.states = ["normal", "true", "false"]
        self.correctList = [[2, 9]]
    }
    
    
    func dragAndDropView3() {
        self.dropList = [
            Model(id: 1 , image: ""  , state: ModelState.empty , linkedId: 1),
            Model(id: 2 , image: ""  , state: ModelState.empty , linkedId: 2),
            Model(id: 3 , image: ""  , state: ModelState.empty , linkedId: 3),
            Model(id: 4 , image: ""  , state: ModelState.empty , linkedId: 4),
            Model(id: 5 , image: "pencils"  , state: ModelState.correct , linkedId: 5),
            Model(id: 6 , image: ""  , state: ModelState.empty , linkedId: 6),
            Model(id: 7 , image: ""  , state: ModelState.empty , linkedId: 7),
            Model(id: 8 , image: ""  , state: ModelState.empty , linkedId: 8),
            Model(id: 9 , image: "cartable"  , state: ModelState.correct , linkedId: 9),
            Model(id: 10 , image: ""  , state: ModelState.empty , linkedId: 10),
            Model(id: 11 , image: ""  , state: ModelState.empty , linkedId: 11),
            Model(id: 12 , image: ""  , state: ModelState.empty , linkedId: 12)
        ]
        self.choicesList = [
            Model(id: 13 , image: "img1 box"  , state: ModelState.normal , linkedId: 13),
            Model(id: 14, image: "img2 box"  , state: ModelState.normal , linkedId: 14),
            Model(id: 15, image: "img3 box"  , state: ModelState.normal , linkedId: 15),
            Model(id: 16, image: "img4 box" , state: ModelState.normal , linkedId: 16)
        ]
        self.correctList = [[16, 12] , [15,2], [14,6], [13,4]]
    }

    
    
    

}
