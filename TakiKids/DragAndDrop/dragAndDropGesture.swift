//
//  dragAndDropGesture.swift
//  TakiKids
//
//  Created by hiba on 11/1/2023.
//

import SwiftUI

struct DraggerI:View {
    @State private var dragOffset = CGSize.zero
    @State var itemDragging: Int = 0
    @State var heartFrame: CGRect? = nil
    @State var heartFrame2: CGRect? = nil
    
    @State var topImage: String = ""
    @State var bottomImage: String = "heart"
    
    var body: some View {
        VStack{
            Image(systemName: "heart.fill")
                .resizable()
                .frame(width: 128, height: 128)
                .overlay(content: {
                    ItemGeometryReader
                })
                .overlay {
                    Image(systemName: topImage)
                        .resizable()
                        .foregroundColor(.red)
                        .frame(width: 128, height: 128)
                        .onTapGesture(count: 2) {
                            print("heyyy")
                            bottomImage = "heart"
                            topImage = ""
                            
                        }
                }
            Image(systemName: bottomImage)
                .resizable()
                .foregroundColor(.red)
                .frame(width: 128, height: 128)
                .overlay(content: {
                    ItemGeometryReader2
                })
                .offset(dragOffset)
                .gesture(DragGesture()
                    .onChanged({ ( value ) in
                        itemDragging = 1
                        // changed
                        self.dragOffset = value.translation
                    })
                    .onEnded({ ( value ) in
                        // ended
                        guard let heartFrame = heartFrame else { return }
                        
                        guard let heartFrame2 = heartFrame2 else { return }

                        let x = value.location.x + heartFrame2.minX
                        let y = value.location.y + heartFrame2.minY
                        
                        if x >= heartFrame.minX
                            && x <= heartFrame.maxX
                            && y >= heartFrame.minY
                            && y <= heartFrame.maxY {
                            topImage = bottomImage
                            bottomImage = ""
                            return
                        }
                        self.dragOffset = CGSize.zero
                    }))
        }
    }
}

extension DraggerI {
    private var ItemGeometryReader: some View {
        GeometryReader { geometry in
            let frame = geometry.frame(in: CoordinateSpace.global)
            Color.clear
                .onChange(of: itemDragging) { newValue in
                    self.heartFrame = frame
                }
        }
    }
    
    private var ItemGeometryReader2: some View {
        GeometryReader { geometry in
            let frame = geometry.frame(in: CoordinateSpace.global)
            Color.clear
                .onChange(of: itemDragging) { newValue in
                    self.heartFrame2 = frame
                }
        }
    }
}
//MARK: View Extension


struct DraggerI_Previews: PreviewProvider {
    static var previews: some View {
        DraggerI()
    }
}
