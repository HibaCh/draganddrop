//
//  DragAndDropView3.swift
//  TakiKids
//
//  Created by hiba on 8/12/2022.
//

import SwiftUI

struct DragAndDropView3: View {
    @State var animationDegree  = 360
    @StateObject var viewModel = DragAndDropViewModel(type: .third)
    let columns = [
        GridItem(.flexible(), alignment: .top),
        GridItem(.flexible(), alignment: .top),
        GridItem(.flexible(), alignment: .top)
    ]
    var body: some View {
        ZStack{
            Image("bg-100")
                .resizable()
                .ignoresSafeArea()
            VStack(alignment: .leading){
                HStack{
                    Image("image")
                    Spacer()
                    ZStack{
                        Image("armoire vide")
                            .rotationEffect(.degrees(Double(animationDegree)))
                            .animation(Animation.linear.repeatCount(Int(3)).speed(5), value: animationDegree)
                            .onChange(of: viewModel.actionAnimation, perform: { _ in
                                if viewModel.actionAnimation {
                                    animationDegree = 358
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                        viewModel.actionAnimation = false
                                        animationDegree = 360
                                    }
                                }
                            })

                            .overlay {
                                LazyVGrid(columns: columns, spacing: 0) {
                                    ForEach(viewModel.dropList, id: \.self) {item  in
                                        DropArea(content: {
                                            ZStack{
                                                Circle()
                                                    .frame(width: 70 , height: 70)
                                                    .opacity(0.01)
                                                Image(item.state == ModelState.empty ? "" : "\(item.image)")
                                            }
                                        }(), item: item, dropList: $viewModel.dropList, choicesList: $viewModel.choicesList)
                                    }
                                }
                                .padding(.trailing, 15)
                                .padding(.top, 15)
                            }
                    }
                    
                }
                HStack{
                    HStack(){
                        ForEach(viewModel.choicesList, id: \.self) {item  in
                            ZStack{
                                Image("box")
                                DragArea(content: {
                                    ZStack{
                                        Image(item.state == ModelState.empty ? "" : "\(item.image)")
                                    }
                                }(), item: item, dropList: $viewModel.dropList, choicesList: $viewModel.choicesList, isClickable: true)
                            }
                        }
                    }
                    Spacer()
                    ZStack{
                        Button {
                            checkWinStateDragAndDrop(dropList: $viewModel.dropList, correctList: viewModel.correctList, actionAnimation: $viewModel.actionAnimation, answersList: $viewModel.answersList )
                        } label: {
                            Image("check_button")
                        }
                    }
                }
            }
            .padding()
            Image("anas")
                .offset(x:50,y:100)
        }
        
    }
}

struct DragAndDropView3_Previews: PreviewProvider {
    static var previews: some View {
        DragAndDropView3()
    }
}
