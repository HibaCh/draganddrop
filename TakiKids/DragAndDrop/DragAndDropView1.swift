//
//  DragAndDropView1.swift
//  TakiKids
//
//  Created by hiba on 24/11/2022. 

import SwiftUI

struct DragAndDropView1: View {
    @StateObject var viewModel = DragAndDropViewModel(type: .first)
    
    var body: some View {
        ZStack{
            Image("background")
                .resizable()
                .ignoresSafeArea()
            VStack(spacing: 25){
                Image("new titre")
                DropList
                ChoicesList
                Button(action: {
                    checkWinStateDragAndDrop(dropList: $viewModel.dropList, correctList: viewModel.correctList, actionAnimation: $viewModel.actionAnimation, answersList: $viewModel.answersList )
                }) {
                    Image("check_button")
                        .font(.system(size: 30))
                }
            }
            
        }
    }
}
extension DragAndDropView1 {
    private var DropList: some View {
        HStack(spacing: 20){
            ForEach(viewModel.dropList, id: \.self) {item  in
                ZStack{
                    Image("state picture")
                    VStack{
                        Image(viewModel.images[item.id-1])
                        DropArea(content: {
                            ZStack{
                                Image(item.state == ModelState.empty ? viewModel.states[0] : item.state == ModelState.normal ? viewModel.states[0] : item.state == ModelState.correct ? viewModel.states[1] : viewModel.states[2])
                                    .opacity(item.state == ModelState.empty ? 0.1 : 1)
                                Image(item.state == ModelState.empty ? "" : "\(item.image)")
                            }
                        }(), item: item, dropList: $viewModel.dropList, choicesList: $viewModel.choicesList)
                        
                    }
                }
            }
        }
    }
    private var ChoicesList: some View {
        HStack{
            ForEach(viewModel.choicesList, id: \.self) {item in
                ZStack{
                    Image("state_boutton")
                    DragArea(content: {
                        ZStack{
                            Image( "\(viewModel.states[0])")
                                .opacity(item.state == ModelState.empty ? 0.1 : 1)
                            Image(item.state == ModelState.empty ? "" : "\(item.image)")
                        }
                    }(), item: item, dropList: $viewModel.dropList, choicesList: $viewModel.choicesList)
                }
            }
        }
    }
}
struct Example1_Previews: PreviewProvider {
    static var previews: some View {
        DragAndDropView1()
    }
}
