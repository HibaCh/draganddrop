//
//  DragAndDropView2.swift
//  TakiKids
//
//  Created by hiba on 29/11/2022.
//

import SwiftUI

struct DragAndDropView2: View {
    @StateObject var viewModel = DragAndDropViewModel(type: .second)
    
    var body: some View {
        ZStack{
            Image("bg-19-19")
                .resizable()
                .ignoresSafeArea()
            VStack(spacing: 25){
                Image("question")
                DropList
                ChoicesList
                Button(action: {
                    checkWinStateDragAndDrop(dropList: $viewModel.dropList, correctList: viewModel.correctList, actionAnimation: $viewModel.actionAnimation, answersList: $viewModel.answersList )
                }) {
                    Image("check_button")
                        .font(.system(size: 30))
                }
            }
            
        }
    }
}

extension DragAndDropView2 {
    private var DropList: some View {
        HStack(spacing: 20){
            ForEach(viewModel.dropList, id: \.self) {item  in
                ZStack{
                    Image("empty2")
                    VStack{
                        DropArea(content: {
                            ZStack{
                                Image(item.state == ModelState.empty ? viewModel.states[0] : item.state == ModelState.normal ? viewModel.states[0] : item.state == ModelState.correct ? viewModel.states[1] : viewModel.states[2])
                                    .opacity(item.state == ModelState.empty ? 0.1 : 1)
                                Image(item.state == ModelState.empty ? "" : "\(item.image)")
                            }
                      
                        
                        }(), item: item, dropList: $viewModel.dropList, choicesList: $viewModel.choicesList)
                    }
                }
            }
        }
    }
    private var ChoicesList: some View {
        HStack(spacing:20){
            ForEach(viewModel.choicesList, id: \.self) {item in
                ZStack{
                    Image("empty2")
                    DragArea(content: {
                        ZStack{
                            Image( "\(viewModel.states[0])")
                                .opacity(item.state == ModelState.empty ? 0.1 : 1)
                            Image(item.state == ModelState.empty ? "" : "\(item.image)")
                        }
                     
                    }(), item: item, dropList: $viewModel.dropList, choicesList: $viewModel.choicesList)
                }
            }
        }
    }
}
struct Example2_Previews: PreviewProvider {
    static var previews: some View {
        DragAndDropView2()
    }
}
