//
//  test1.swift
//  TakiKids
//
//  Created by hiba on 22/11/2022.

import SwiftUI
//import PlaygroundSupport
import Combine

protocol middleManProtocol {
    func swapPerson(myName:String, nameMovingElement:String)->Bool
}

class MiddleMan:ObservableObject{
    @Published var people = [PersonRow]()
}

struct Person: Identifiable, Hashable {
    var id = UUID()
    var name: String
}

struct PersonRow: View, Identifiable {
    var id = UUID()
    var person: Person
    @State var targeted: Bool = true
    var delegate: middleManProtocol
    @State var positionOnScroll:CGPoint = .zero
    var body: some View {
        HStack() {
                Text(person.name)
            }
            .onDrop(of: ["public.utf8-plain-text"], isTargeted: self.$targeted,
                    perform: { (provider) -> Bool in
                        guard let personSwaping = provider.first, let name = personSwaping.suggestedName
                            else { return false}
                
                        return self.delegate.swapPerson(myName: self.person.id.uuidString,
                                                        nameMovingElement: name)

            })
            .onDrag {
                let item = NSItemProvider(object: NSString(string: self.person.name))
                item.suggestedName = self.person.id.uuidString
                return item
            }
    }
}

struct test1 : View, middleManProtocol{
    
    func swapPerson(myName:String, nameMovingElement:String)->Bool{
//        print(self.delegate2.people.firstIndex(where: {$0.person.id.uuidString == myName}))
//        print(myName)
        print("swap running")

        //Get index of person (me) and get the index of the one moving
        guard let indexOfElementMoving = self.delegate.people.firstIndex(where: {$0.person.id.uuidString == nameMovingElement}) else {
            return false}
        print("got index of moving provider", indexOfElementMoving)
        
        guard let myIndex = self.delegate.people.firstIndex(where: {$0.person.id.uuidString == myName}) else {return false}
        print("got my index", myIndex)
        //Swap
        
        self.delegate.people.swapAt(myIndex, indexOfElementMoving)
      

        return true
    }
    

    @ObservedObject var delegate: MiddleMan = MiddleMan()
    @ObservedObject var delegate2: MiddleMan = MiddleMan()
   // @State var targeted: Bool = true

    var body: some View {
        ScrollView(.vertical, showsIndicators: true){
           VStack{
               VStack(spacing: 3 ) {
                    ForEach(delegate.people) { person in
                        person
                             .padding()
                           
                             .background{
                                 RoundedRectangle(cornerRadius: 6, style: .continuous)
                                     .stroke(.gray)
                             }
                             .background{
                                 RoundedRectangle(cornerRadius: 6, style: .continuous)
                                     .fill(person.person.name != ""  ? .gray.opacity(0.25)  :
                                             .clear )
                             }
                            .frame(width:100 , height:70)

                    }
                    
                }
//               HStack(spacing: 30) {
//                    ForEach(delegate2.people) { person in
//                        person
//                            .padding()
//                            .frame(width: 60 , height: 30)
//
//                            .background{
//                                RoundedRectangle(cornerRadius: 6, style: .continuous)
//                                    .fill(person.person.name != "" ? .clear :
//                                            .gray.opacity(0.25))
//                            }
//                            .background{
//                                RoundedRectangle(cornerRadius: 6, style: .continuous)
//                                    .stroke(.gray)
//                            }
//                            .frame(width:100 , height:70)
//
//                    }
//
//                }
           }
            
        }.onAppear {
            self.delegate2.people = [
            PersonRow(person: Person(name: " "), delegate: self),
//            PersonRow(person: Person(name: "c"), delegate: self),
//            PersonRow(person: Person(name: "b"), delegate: self),
//            PersonRow(person: Person(name: "f"), delegate: self),
//            PersonRow(person: Person(name: "k"), delegate: self)
            ]
            //Do your call to server or model  or whatever
            //I don't like the delegate setter like this, pretty sure i can create a better approach, just doing it like this for now.
            self.delegate.people = [
                PersonRow(person: Person(name: "George"), delegate: self),
                PersonRow(person: Person(name: "Michael"), delegate: self),
            PersonRow(person: Person(name: "Wilson"), delegate: self),
            PersonRow(person: Person(name: "Jocko"), delegate: self),
            PersonRow(person: Person(name: "Jhon"), delegate: self),
                
                PersonRow(person: Person(name: " "), delegate: self),
                PersonRow(person: Person(name: " "), delegate: self),
                PersonRow(person: Person(name: " "), delegate: self),
                PersonRow(person: Person(name: " "), delegate: self),
                PersonRow(person: Person(name: " "), delegate: self)           ]
            
        }
        
    }

    
    
}

// Present the view controller in the Live View window
//PlaygroundPage.current.setLiveView(MyView())
