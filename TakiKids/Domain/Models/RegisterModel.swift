//
//  RegisterModel.swift
//  TakiKids
//
//  Created by hiba on 19/12/2022.
//

import Foundation
struct RegisterModel : Codable {
    let name, lastName, phoneNumber, email: String
    let address, plainPassword, countries: String
}
