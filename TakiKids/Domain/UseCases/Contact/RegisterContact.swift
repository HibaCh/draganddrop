import Foundation
class RegisterContact : RegisterContactUseCaseProtocol{
    
    
    private let contactRepo: ContactRepositoryProtocol
    
    init(contactRepo: ContactRepositoryProtocol){
        self.contactRepo = contactRepo
    }
    
    func execute(_ contact: RegisterModel) async -> Result<Bool, ContactError> {
        return await self.contactRepo.registerContact(contact)
    }
}
