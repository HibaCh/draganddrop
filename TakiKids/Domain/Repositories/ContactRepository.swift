import Foundation

class ContactRepository : ContactRepositoryProtocol{
    private let contactDataSource: ContactDataSourceProtocol

    init(contactDataSource: ContactDataSourceProtocol){
        self.contactDataSource = contactDataSource
    }
    
    func registerContact(_ contactRequestModel: RegisterModel) async  -> Result<Bool, ContactError> {
        return await contactDataSource.register(contactRequestModel)
    }
}
