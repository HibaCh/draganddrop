import Foundation

protocol ContactRepositoryProtocol{
    func registerContact(_ contactRequestModel: RegisterModel) async -> Result<Bool, ContactError>
}
