//
//  RegisterContactUseCaseProtocol.swift
//  TakiKids
//
//  Created by hiba on 27/12/2022.
//

import Foundation
protocol RegisterContactUseCaseProtocol {
    func execute(_ contact: RegisterModel) async -> Result<Bool, ContactError>
}
