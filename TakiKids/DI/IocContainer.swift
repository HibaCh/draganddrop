//
//  IocContainer.swift
//

import Foundation
import Swinject

func buildContainer() -> Container {
    let container = Container()
    
    
    
    
    container.register(AuthRepositoryProtocol.self){ _ in
        return AuthRepository(authDataSource : container.resolve(AuthDataSourceProtocol.self)!)
    }.inObjectScope(.container)

    container.register(RegisterUseCaseProtocol.self){ _ in
        return RegisterUseCase(registerRepo: container.resolve(AuthRepositoryProtocol.self)!)
    }.inObjectScope(.container)
    
    return container
    
}
