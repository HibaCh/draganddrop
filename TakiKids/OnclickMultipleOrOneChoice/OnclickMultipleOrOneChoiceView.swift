//
//  OnclickMultipleOrOneChoiceView.swift
//  TakiKids
//
//  Created by hiba on 7/12/2022.
//

import SwiftUI
struct OnclickMultipleOrOneChoiceView: View {
    @StateObject var viewModel = ClickViewModel()
    @State var answersList: [Int] = []
    var isMultiChoices : Bool {
        if viewModel.correctList.count > 1 {
            return true
        }
        return false
    }
    var body: some View {
        ZStack{
            Image("bg1-100")
                .resizable()
                .ignoresSafeArea()
            VStack(alignment: .center ){
                Image("question")
                    .offset(x:-150)
                ChoicesList
                ZStack{
                    Button {
                       checkWinState(choicesList: $viewModel.choicesList, answersList: answersList, correctList: viewModel.correctList)
                    } label: {
                        Image("check_button")
                    }
                }
                .offset(x:300)
                
            }
        }
        
    }
}


extension OnclickMultipleOrOneChoiceView {
    private var ChoicesList: some View {
        HStack{
            VStack(spacing: 30){
                ForEach(viewModel.choicesList, id: \.self) { item in
                    ZStack{
                        Image(item.state == ModelState.empty ? viewModel.states[0] : item.state == ModelState.normal ? viewModel.states[1] : item.state == ModelState.correct ? viewModel.states[2] : viewModel.states[3])
                        Image(item.image)
                    }
                    .onTapGesture{
                        OnclickMultipleOrOneChoice(isMultiChoices: isMultiChoices,  answersList: $answersList, item: item, choicesList: $viewModel.choicesList)
                    }
                }
            }
        }.padding(.trailing)
            .offset(x:150)
    }
}

struct OnclickMultipleOrOneChoiceView_Previews: PreviewProvider {
    static var previews: some View {
        OnclickMultipleOrOneChoiceView()
    }
}



