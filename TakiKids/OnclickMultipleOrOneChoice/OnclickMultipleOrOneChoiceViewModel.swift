//
//  ClickViewModel.swift
//  TakiKids
//
//  Created by hiba on 7/12/2022.
//
import Foundation
class ClickViewModel: ObservableObject {
    @Published var isWin : Bool = false
//    @Published var dropList: [Model] = []
    @Published var choicesList: [Model] = [
        Model(id: 1 , image: "texte_1" , state: ModelState.empty),
        Model(id: 2 , image: "texte_2"  , state: ModelState.empty),
        Model(id: 3 , image: "texte_3", state: ModelState.empty)
    ].shuffled()
    @Published var states: [String] = ["state_1", "state_2", "state_3" , "state_4"]
    @Published var correctList: [Int] = [3]


}
