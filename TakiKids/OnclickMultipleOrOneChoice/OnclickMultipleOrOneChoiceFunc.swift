//
//  OnclickMultipleOrOneChoiceFunc.swift
//  TakiKids
//
//  Created by hiba on 7/12/2022.
//

import Foundation
import SwiftUI

func OnclickMultipleOrOneChoice (isMultiChoices: Bool, answersList: Binding<[Int]>, item: Model, choicesList: Binding<[Model]>) {
    let index = choicesList.firstIndex(where: {$0.id == item.id})!
    //check if the onClick is multiple
    if isMultiChoices{
      //if the state is wrong they can't modifie it and remove it from the answer array
        choicesList.map{ i in
            if i.wrappedValue.state == ModelState.wrong{
                if let index = answersList.wrappedValue.firstIndex(where: {$0 == i.id}) {
                    answersList.wrappedValue.remove(at: index)
                }
            }
        }
        //chose answers
        if item.state != ModelState.correct  && item.state != ModelState.wrong {
            choicesList[index].wrappedValue.state = ModelState.empty
            if let index = answersList.wrappedValue.firstIndex(where: {$0 == item.id}) {
                answersList.wrappedValue.remove(at: index)
            }else{
                choicesList[index].wrappedValue.state = ModelState.normal
                answersList.wrappedValue.append(item.id)
            }
        }
    }
    //check if the onClick is single
    else{
        if item.state != ModelState.wrong && item.state != ModelState.correct
        {
            answersList.wrappedValue.removeAll()
            choicesList.map { i in
                if  i.wrappedValue.state == ModelState.normal{ i.wrappedValue.state = ModelState.empty}
            }
            answersList.wrappedValue.append(item.id)
            choicesList[index].wrappedValue.state = ModelState.normal
        }
        
    }
}

func checkWinState(choicesList: Binding<[Model]>, answersList:[Int], correctList: [Int]) {
    if answersList.sorted() == correctList.sorted() || areArraysEquivalent(first: answersList, second: correctList){
       // isWin = true
        SoundManger.instance.playSound(sound: .tada)
    }else{
        SoundManger.instance.playSound(sound: .badum)
    }
    for answer in answersList {
        if let index = choicesList.firstIndex(where: {$0.id == answer}){
            let item = choicesList[index]
            choicesList[index].state.wrappedValue = ModelState.wrong
            if correctList.contains(item.id){
                choicesList[index].state.wrappedValue = ModelState.correct
            }
        }
    }
}
