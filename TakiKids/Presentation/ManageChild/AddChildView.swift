//
//  AddChildView.swift
//  TakiKids
//
//  Created by hiba on 3/1/2023.
//

import SwiftUI
func dataOfToday(date:Date) -> LocalizedStringKey{
    let df = DateFormatter()
    df.dateFormat = "yyyy-MM-dd"
    let dateString = df.string(from: date)
    return LocalizedStringKey(dateString)
}
struct AddChildView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State var name : String = ""
    @State var selectedDate : Date = Date()
    @State private var isRegular = false
    @State var showsDatePicker :Bool = false
    @State var birthday = LocalizedStringKey("Birthday")
    @State var dob = Date()
    @State private var isExpanded = false
    @State  var SexChildArray = ["boy" ,"girl"]
    @State  var SexChild = "sexCild"
    
    
    
    var body: some View {
        ZStack{
            //Background
            LinearGradient(gradient: Gradient(colors: [Color("AuthBackgroundFirstGradient"), Color("AuthBackgroundSecondGradient")]), startPoint: .topLeading, endPoint: .bottomTrailing)
                .ignoresSafeArea()
            
            // Game Logo...
            Image("logo")
                .resizable()
                .frame(width: 50 , height: 50)
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
                .padding(.trailing, 20)
                .padding(.top, 20)
            
            //logo Taki kids
            Image("logo")
                .resizable()
                .frame(width: 50 , height: 50)
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
                .padding(.trailing, 20)
                .padding(.top, 20)
            
            backButton
            
            GeometryReader { geometry in
                ScrollView {
                    VStack(spacing: 5) {
                        //register filds
                        RegisterFilds
                            .zIndex(5)
                        //register button
                        RegisterButton
                            .zIndex(2)
                    }
                    .padding(.all, 20)
                    .padding(.trailing, 60)
                    .frame(maxWidth: geometry.size.width, minHeight: geometry.size.height)
                }
            }
            
            if showsDatePicker{
                AlertCalanderView(birthday: $birthday, showsDatePicker: $showsDatePicker)
            }
            
        }
    }
}

extension AddChildView {
    private var backButton: some View {
        ZStack {
            Image("arrow")
                .resizable()
                .frame(width: 40, height: 40)
                .foregroundColor(.blue)
                .imageScale(.large)
                .padding()
                .onTapGesture {
                    presentationMode.wrappedValue.dismiss()
                }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        .padding(.top, 20)
    }
    private var SexChildListDropDown: some View {
        VStack {
            ScrollView {
                VStack{
                    ForEach(SexChildArray , id: \.self){sex in
                        Text(LocalizedStringKey(sex))
                            .frame(width:300)
                            .onTapGesture {
                                SexChild = sex
                                isExpanded = false
                            }
                    }
                }
                .foregroundColor(Color("AuthPlaceHolderColor"))
                .font(.system(size: 20))
            }
            .frame(width: (UIScreen.main.bounds.width - 90) * 0.42 )
        }
        .environment(\.layoutDirection, isRegular ? .leftToRight : .rightToLeft)
        .frame(height: 50)
        .padding(.all)
        .frame(width: (UIScreen.main.bounds.width - 90) * 0.42)
        .background(.white)
        .cornerRadius(20)
    }
    private var RegisterButton: some View {
        Button {
            
        } label: {
            Text(LocalizedStringKey("register"))
                .font(.system(size: 20))
                .foregroundColor(.white)
                .fontWeight(.bold)
                .padding()
                .background(
                    Rectangle()
                        .foregroundColor(.yellow)
                        .cornerRadius(50)
                )
        }
    }
    private var RegisterFilds : some View{
        VStack {
            HStack(spacing:30){
                
                VStack(alignment:.leading, spacing:10) {
                    CustomTextField(name: name, placeholder: "childName")
                    CustomTextField(name: name, placeholder: "childLastName")
                    CustomField(text: birthday, show: $showsDatePicker)
                    CustomField(text: LocalizedStringKey("سنة أولى ") , show: .constant(true))
                }
                
                VStack(spacing:30){
                    Image("addPhotoSon")
                        .resizable()
                        .frame(width: 150 , height: 150)
                    CustomField(text: LocalizedStringKey(SexChild) , show: $isExpanded)
                        .overlay {
                            if isExpanded {
                                SexChildListDropDown
                                    .offset(y: 70)
                            }
                        }
                }
            }
            
        }
        .environment(\.layoutDirection, isRegular ? .leftToRight : .rightToLeft)
    }
}

struct CustomTextField: View {
    @State var name : String
    @State var placeholder : String
    @State private var isRegular = false
    
    var body: some View {
        VStack(alignment: .trailing, spacing: 0) {
            Image("emprt_box1")
                .resizable()
                .frame(width: (UIScreen.main.bounds.width - 90) * 0.42 , height: 50)
                .overlay {
                    TextField( "" , text: $name)
                        .padding(.trailing, 60)
                        .placeholder(when: name.isEmpty) {
                            Text(LocalizedStringKey(placeholder))
                        }
                        .foregroundColor(Color("AuthPlaceHolderColor"))
                    //.font(.custom("Cairo-Regular", size: 14))
                        .environment(\.layoutDirection, isRegular ? .leftToRight : .rightToLeft)
                        .offset(x: 20, y: -2.5)
                }
        }
    }
}
struct CustomField: View {
    var text: LocalizedStringKey
    @Binding var show : Bool
    var body: some View {
        HStack {
            ZStack{
                Image("emprt_box1")
                    .resizable()
                    .frame(width: (UIScreen.main.bounds.width - 90) * 0.42 , height: 50)
                Text(text)
                    .foregroundColor(Color("AuthPlaceHolderColor"))
//                    .offset(x:-120)
            }
            .onTapGesture {
                show.toggle()
                
            }
        }
    }
}

struct AddChildView_Previews: PreviewProvider {
    static var previews: some View {
        AddChildView()
    }
}
