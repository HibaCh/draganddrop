//
//  BirthdayFieldView.swift
//  DatePicker
//
//  Created by hiba on 4/1/2023.
//

import SwiftUI

//struct BirthdayFieldView: View {
//    @State var birthday : LocalizedStringKey
//    @State var showsDatePicker :Bool
//    var body: some View {
//            HStack {
//                ZStack{
//                    Image("emprt_box1")
//                    Text(birthday)
//                }
//                .onTapGesture {
//                    self.showsDatePicker.toggle()
//                    print(showsDatePicker)
//                }
//                .padding(EdgeInsets(top: 2, leading: 10, bottom: 2, trailing: 10))
//                .background(showsDatePicker
//                            ? Color.clear
//                            : Color.clear)
//            }
//    }
//}

struct AlertCalanderView: View {
    @Binding var birthday : LocalizedStringKey
    @State var dob = Date()
    @Binding var showsDatePicker : Bool
    
    var body: some View{
        ZStack (alignment: Alignment(horizontal: .trailing, vertical: .top)) {
            VStack {
                VStack{
                    DatePicker("", selection: $dob, displayedComponents: .date)
                        .datePickerStyle(WheelDatePickerStyle())
                    HStack{
                        Button {
                            birthday = dataOfToday(date: dob)
                            showsDatePicker = false
                        } label: {
                            Text("حفظ")
                        }
                        Spacer()
                        Button {
                            showsDatePicker = false
                        } label: {
                            Text("إلغاء")
                        }
                    }
                }
            }
            .frame(width: UIScreen.main.bounds.width * 0.3, height: UIScreen.main.bounds.height * 0.45)
            .padding(.vertical, 25)
            .padding(.horizontal, 30)
            .background(Color.white)
            .cornerRadius(25)
            
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.black.opacity(0.75))
        .ignoresSafeArea(.all)
    }
}




//struct BirthdayFieldView_Previews: PreviewProvider {
//    static var previews: some View {
//        BirthdayFieldView(birthday: "", showsDatePicker: false)
//    }
//}
