//
//  CustomTextFieldModifier.swift
//  TakiKids
//
//  Created by Ahlem Zanina on 26/12/2022.
//

import Foundation
import SwiftUI

struct CustomTextFieldModifier: ViewModifier {
    @State var isPassword: Bool = false
    @State var isRegular: Bool
    @State var placeholder: String
    @Binding var text: String
    
    func body(content: Content) -> some View {
        content
            .padding(.trailing, isPassword ? 60 : 30)
            .placeholder(when: text.isEmpty) {
                Text(LocalizedStringKey(placeholder))
            }
            .foregroundColor(Color("AuthPlaceHolderColor"))
            .font(.custom("Cairo-Regular", size: 14))
            .environment(\.layoutDirection, isRegular ? .leftToRight : .rightToLeft)
            .offset(x: -20, y: -2.5)
    }
}
