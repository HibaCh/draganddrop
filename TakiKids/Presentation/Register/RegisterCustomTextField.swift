//
//  RegisterCustomTextField.swift
//  TakiKids
//
//  Created by Ahlem Zanina on 22/12/2022.
//

import SwiftUI

struct RegisterCustomTextField: View {
    @State var isRegular: Bool = false
    @State var isPassword: Bool = false
    @State var placeholder: String
    @Binding var text: String
    @Binding var errorText: LocalizedStringKey
    @Binding var isSecureField: Bool
    var onChange: () -> Void
    
    var body: some View {
        VStack(alignment: .trailing, spacing: 0) {
            Image(getInputState(text: text, errorMessage: errorText))
                .resizable()
                .frame(width: (UIScreen.main.bounds.width - 90) * 0.42 , height: 50)
                .overlay {
                    if isSecureField {
                        SecureField( "" , text: $text)
                            .onChange(of:text, perform: { _ in
                                onChange()
                            })
                            .modifier(CustomTextFieldModifier(isPassword: true, isRegular: isRegular, placeholder: placeholder, text: $text))
                        
                    } else {
                        TextField( "" , text: $text)
                            .onChange(of:text, perform: { _ in
                                onChange()
                            })
                            .modifier(CustomTextFieldModifier(isRegular: isRegular, placeholder: placeholder, text: $text))
                    }
                }
                .overlay(passwordIcon, alignment: .leading)
            
            errorMessage
        }
    }
}

extension RegisterCustomTextField {
    private var passwordIcon: some View {
        Image(systemName: isSecureField ? "eye.slash" : "eye")
            .resizable()
            .scaledToFit()
            .frame(width: 20)
            .padding(.all, 8)
            .foregroundColor(.blue)
            .offset(x: 10, y: -2.5)
            .opacity(isPassword ? 1 : 1)
            .onTapGesture {
                if isPassword {
                    isSecureField.toggle()
                }
            }
    }
    
    private func getInputState(text: String, errorMessage: LocalizedStringKey) -> String {
        if text.isEmpty && errorMessage == "" {
            return "AuthStateBlue"
        } else if errorMessage != "" {
            return "AuthStateRed"
        } else {
            return "AuthStateGreen"
        }
    }

    private var errorMessage: some View {
        HStack(alignment: .center) {
            Text(errorText)
                .font(.caption)
                .foregroundColor(Color("AuthErrorColor"))
                .font(.custom("Cairo-Regular", size: 14))
                .frame(height: 10)
            Image("error_message")
                .resizable()
                .frame(width: 18, height: 18)
                .opacity(errorText != "" ? 1 : 0)
        }
        .padding(.horizontal, 15)
        .padding(.bottom, 5)
        .padding(.top, 2)
    }
}

struct RegisterCustomTextField_Previews: PreviewProvider {
    static var previews: some View {
        RegisterCustomTextField(
            placeholder: "emailOrNumber",
            text: .constant(""),
            errorText: .constant(LocalizedStringKey("checkField")),
            isSecureField: .constant(true),
            onChange: {}
        )
    }
}
