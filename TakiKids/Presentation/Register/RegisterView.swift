//
//  RegisterView.swift
//  TakiKids
//
//  Created by hiba on 15/12/2022.
//

import SwiftUI



struct RegisterView: View {
    @StateObject var viewModel = ViewModelRegister()
    var countryItems : [Country]{
        var countryItems = CountryData
        search == ""
        ? (countryItems = countryItems.map{$0})
        
        : (countryItems = countryItems.map{$0}.filter
           {
            $0.name.lowercased().contains(search.lowercased())
        })
        return countryItems
    }
    @State private var isExpanded = false
    @State private var selectedName = "تونس"
    @State private var selectedFlag = countryFlag(countryCode: "TN")
    @State private var showModel = false
    @State private var isSecureField : Bool = true
    @State private var isRegular = false
    @State private var search = ""
    let columns = [
        GridItem(.flexible(), alignment: .top),
        GridItem(.flexible(), alignment: .top),
    ]
    @State var name : String = ""
    var body: some View {
        ZStack{
            //background
            Rectangle()
                .ignoresSafeArea()
                .foregroundColor(Color(red: 0.4117647058823529, green: 0.8745098039215686, blue: 0.9764705882352941))
        
            ScrollView{
                HStack{
                    ZStack{
                        VStack(spacing:10){
                            //register filds
                            RegisterFilds
                            //navigation to login view
                            NavigationLoginButton
                            //register button
                            RegisterButton
                        }
                        //countries list
                        if isExpanded{
                            Country
                                .frame(maxWidth: .infinity , maxHeight: .infinity , alignment: .trailing)
                                .offset(x:-20, y: -5)
                        }
                    }
                    //logo Taki kids
                }
                .padding()
            }
        }
    }
}

extension RegisterView {
    private var NavigationLoginButton: some View {
        HStack{
            Text(LocalizedStringKey("login"))
                .foregroundColor(.yellow)
                .fontWeight(.bold)
            Text(LocalizedStringKey("youHaveAccount"))
                .foregroundColor(.white)
                .fontWeight(.bold)
        }
        .font(.system(size: 20
                     ))
    }
    private var RegisterButton: some View {
        Button {
            viewModel.signUp()
        } label: {
            Text(LocalizedStringKey("register"))
                .font(.system(size: 20))
                .foregroundColor(.white)
                .fontWeight(.bold)
                .padding()
                .background(
                    Rectangle()
                        .foregroundColor(.yellow)
                        .cornerRadius(50)
                )
        }
    }
    private var Country: some View {
        VStack{
            HStack{
                Image(systemName: "magnifyingglass" )
                TextField("بحث", text: $search)
            }
            .foregroundColor(.blue)
            .padding(5)
            .overlay(
                RoundedRectangle(cornerRadius:10)
                    .stroke(.blue)
            )
            ScrollView{
                VStack{
                    ForEach(countryItems) { country in
                        HStack{
                            Text(countryFlag(countryCode: country.code))
                            Text(country.name)
                                .foregroundColor(.blue)
                            Spacer()
                        }
                        .onTapGesture {
                            self.selectedFlag = countryFlag(countryCode: country.code)
                            self.selectedName = country.name
                            withAnimation {
                                self.isExpanded.toggle()
                            }
                        }
                    }
                }
            }
        }
        .environment(\.layoutDirection, isRegular ? .leftToRight : .rightToLeft)
        .frame(height: 150)
        .frame(width: UIScreen.main.bounds.width * 0.31)
        .accentColor(.blue)
        .foregroundColor(.blue)
        .padding(.all)
        .background(.white)
        .cornerRadius(20)
        .padding(.all)
    }
    private var RegisterFilds : some View{
        //country button
        LazyVGrid(columns: columns, spacing: 20) {
            Button {
                withAnimation {
                    self.isExpanded.toggle()
                }
            } label: {
                HStack{
                    Text( selectedFlag)
                    Text( selectedName)
                        .foregroundColor(.blue)
                        .multilineTextAlignment(.trailing)
                    Spacer()
                }
            }
            .modifier(StyleOfButton(showPlaceHolder: name.isEmpty, placeholder:LocalizedStringKey("")))
            VStack(alignment:.leading) {
                TextField( "" , text:selectedName.contains("تونس") ? $viewModel.phoneNumber : $viewModel.email )
                    .modifier(StyleOfButton(showPlaceHolder: selectedName.contains("تونس") ? viewModel.phoneNumber.isEmpty : viewModel.email.isEmpty, placeholder:LocalizedStringKey( selectedName.contains("تونس") ? "parentPhone" : "parentEmail")))
                    .keyboardType(selectedName.contains("تونس") ? .numberPad : .emailAddress)
                if viewModel.testfunc{
                    Text(selectedName.contains("تونس") ? viewModel.PhoneNumberPrompt :viewModel.emailPrompt)
                        .fixedSize(horizontal: false, vertical: true)
                        .font(.caption)
                        .foregroundColor(.red)
                        .padding(.bottom , -10)
                    
                }
            }
            VStack(alignment:.leading) {
                TextField( "" , text: $viewModel.name)
                    .modifier(StyleOfButton(showPlaceHolder: viewModel.name.isEmpty, placeholder:LocalizedStringKey("parentName")))
                if viewModel.testfunc{
                    Text(viewModel.namePrompt)
                        .fixedSize(horizontal: false, vertical: true)
                        .font(.caption)
                        .foregroundColor(.red)
                        .padding(.bottom , -10)
                }
            }
            VStack(alignment:.leading){
                TextField( "" , text: $viewModel.lastName)
                    .modifier(StyleOfButton(showPlaceHolder: viewModel.lastName.isEmpty, placeholder:LocalizedStringKey("parentLastName")))
                if viewModel.testfunc{
                    Text(viewModel.lastNamePrompt)
                        .fixedSize(horizontal: false, vertical: true)
                        .font(.caption)
                        .foregroundColor(.red)
                        .padding(.bottom , -10)
                }
            }
            VStack(alignment:.leading){
                eyeFild(isSecureField: $isSecureField, fildName: $viewModel.plainPassword, LocalizedFildName: "password")
                if viewModel.testfunc{
                    Text(viewModel.passwordPrompt)
                        .fixedSize(horizontal: false, vertical: true)
                        .font(.caption)
                        .foregroundColor(.red)
                        .padding(.bottom , -10)
                }
            }
            VStack(alignment:.leading){
                eyeFild(isSecureField: $isSecureField, fildName: $viewModel.confirmPassword, LocalizedFildName: "confirmPassword")
                if viewModel.testfunc{
                    Text(viewModel.confirmPwPrompt)
                        .fixedSize(horizontal: false, vertical: true)
                        .font(.caption)
                        .foregroundColor(.red)
                        .padding(.bottom , -10)
                }
            }
            
        }
        .environment(\.layoutDirection, isRegular ? .leftToRight : .rightToLeft)
    }
}


struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
