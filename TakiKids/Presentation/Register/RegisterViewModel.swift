//
//  RegisterViewModel.swift
//  TakiKids
//
//  Created by hiba on 19/12/2022.
//

import Foundation
import SwiftUI
class ViewModelRegister: ObservableObject {
    
//    private let registerContactUseCase: RegisterContactUseCaseProtocol
//    init(registerContact: RegisterContactUseCaseProtocol){
//        self.registerContactUseCase = registerContact
//    }
    
    @Published var countries: String = ""
    @Published var name: String = ""
    @Published var lastName: String = ""
    @Published var  phoneNumber : String = ""
    @Published var email: String = ""
    @Published var address : String = ""
    @Published var plainPassword: String = ""
    @Published var confirmPassword: String = ""
    @Published var testfunc : Bool = false
    
 
    
    
    func passwordsMatch() -> Bool {
        plainPassword == confirmPassword
      }
      
      func isPasswordValid() -> Bool {
          plainPassword.count  > 8
      }
      
      func isEmailValid() -> Bool {
          // criteria in regex.  See http://regexlib.com
          let emailTest = NSPredicate(format: "SELF MATCHES %@",
                                      "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$")
          return emailTest.evaluate(with: email)
      }
      
    func isValidName() -> Bool {
        name.count > 3 && !name.starts(with: " ")
    }
     
    func isValidLastName() -> Bool {
        lastName.count > 3 && !lastName.starts(with: " ")
    }
    
    func isValidNumberPhone() -> Bool {
        phoneNumber.count == 8 && phoneNumber.isNumber
       
    }
    
    
      var isSignUpComplete: Bool {
          if   !isValidName() ||
          !passwordsMatch() ||
          !isPasswordValid() ||
          !isEmailValid() ||
          !isValidLastName() ||
          !isValidNumberPhone() {
              return false
          }
          return true
      }
      
      // MARK: - Validation Prompt Strings
      var confirmPwPrompt: LocalizedStringKey {
          if passwordsMatch() {
              return ""
          } else {
              return LocalizedStringKey("confirmPwError")
          }
      }
      
      var emailPrompt: LocalizedStringKey {
          if isEmailValid() {
              return ""
          } else {
              return LocalizedStringKey("emailError")
          }
      }
      
      var passwordPrompt: LocalizedStringKey {
          if isPasswordValid() {
              return ""
          } else {
              return LocalizedStringKey("passwordError")
          }
      }
      
    var namePrompt: LocalizedStringKey {
        if isValidName() {
            return ""
        } else {
            return LocalizedStringKey("firstNameError")
        }
    }
 
    var lastNamePrompt: LocalizedStringKey {
        if isValidLastName() {
            return ""
        } else {
            return LocalizedStringKey("lastNameError")
        }
    }
    
    
    var PhoneNumberPrompt: LocalizedStringKey {
        if isValidNumberPhone() {
            return ""
        } else {
            return LocalizedStringKey("phoneError")
        }
    }
    
    func signUp() {
        testfunc = true
    }
}
