//
//  countryView.swift
//  TakiKids
//
//  Created by hiba on 15/12/2022.
//

import SwiftUI



struct countryView: View {
    var country: [Country] = CountryData
    @State private var isExpanded = false
    @State private var selectedName = "تونس"
    @State private var selectedFlag = countryFlag(countryCode: "TN")
    var body: some View {
        VStack{
                ScrollView{
                    VStack{
                        ForEach(country) { country in
                                HStack{
                                    Text( countryFlag(countryCode: country.code))
                                    Text(country.name)
                                        .multilineTextAlignment(.trailing)
                                        Spacer()
                                }
                                .onTapGesture {
                                    self.selectedFlag = countryFlag(countryCode: country.code)
                                    self.selectedName = country.name
                                    withAnimation {
                                        self.isExpanded.toggle()
                                    }
                                }
                        }
                    }
                }.frame(height: 150)
            .frame(width: UIScreen.main.bounds.width * 0.32)
            .accentColor(.blue)
            .foregroundColor(.blue)
            .padding(.all)
            .background(.white)
            .cornerRadius(5)
        }.padding(.all)
        }
}

struct countryView_Previews: PreviewProvider {
    static var previews: some View {
        countryView()
    }
}


/*struct countryView: View {
 var country: [Country] = CountryData
 @State private var isExpanded = false
 @State private var selectedName = "تونس"
 @State private var selectedFlag = countryFlag(countryCode: "TN")
 var body: some View {
     VStack{
         DisclosureGroup("\(selectedName) \(selectedFlag) ", isExpanded: $isExpanded){
             ScrollView{
                 VStack{
                     ForEach(country) { country in
                             HStack{
                                 Text( countryFlag(countryCode: country.code))
                                 Text(country.name)
                                     .multilineTextAlignment(.trailing)
                                     Spacer()
                             }
                             .onTapGesture {
                                 self.selectedFlag = countryFlag(countryCode: country.code)
                                 self.selectedName = country.name
                                 withAnimation {
                                     self.isExpanded.toggle()
                                 }
                             }
                     }
                 }
             }.frame(height: 150)
         }
         .frame(width: UIScreen.main.bounds.width * 0.32)
         .accentColor(.blue)
         .foregroundColor(.blue)
         .padding(.all)
         .background(.white)
         .cornerRadius(50)
     }.padding(.all)
     }
}*/
