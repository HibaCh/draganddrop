//
//  VerifiedCodeView.swift
//  TakiKids
//
//  Created by hiba on 26/12/2022.
//

import SwiftUI

struct VerifiedCodeView: View {
    @State var otpText: String = ""
    @FocusState private var iskeyboardShowing : Bool
    @State var test :Bool = false
    var body: some View {
        ZStack{
            //Background
            LinearGradient(gradient: Gradient(colors: [Color("AuthBackgroundFirstGradient"), Color("AuthBackgroundSecondGradient")]), startPoint: .topLeading, endPoint: .bottomTrailing)
                .ignoresSafeArea()
            // Game Logo...
            Image("logo")
                .resizable()
                .frame(width: 50 , height: 50)
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
                .padding(.trailing, 20)
                .padding(.top, 20)
            
            GeometryReader { geometry in
                ScrollView{
                    VStack{
                        Text(LocalizedStringKey("enterYourCode"))
                            .bold()
                            .padding(.bottom , 30)
                            .foregroundColor(.white)
                        //code fields
                        CodeFields
                        //reset code button
                        ResetCodeButton
                        //confirm code button
                        ConfirmButton
                    }
                    .padding(.all, 20)
                    .padding(.trailing, 60)
                    .frame(maxWidth: geometry.size.width, minHeight: geometry.size.height)
                }
            }
        }
    }
    
    @ViewBuilder
    func OTPTextBox(_ index: Int)-> some View {
        ZStack{
            Image("normal_box")
            if otpText.count > index {
                let startIndex = otpText.startIndex
                let charIndex = otpText.index(startIndex, offsetBy: index)
                let charTOString = String(otpText[charIndex])
                Text(charTOString)
                    .foregroundColor(.blue)
                    .font(.system(size: 25))
            }else{
                Text("")
//                    .foregroundColor(.blue)
//                    .font(.system(size: 25))
            }
        }
        
    }
}

extension VerifiedCodeView {
    private var CodeFields: some View {
        HStack{
            ForEach(0..<6,id: \.self){index in
                OTPTextBox(index)
            }
        }
        .overlay{
            TextField("ghghgf", text: $otpText.limit(6, test: $test))
                .keyboardType(.numberPad)
                .textContentType(.oneTimeCode)
//                .frame(width: 1,height: 1)
                .opacity(0.001)
//                .blendMode(.screen)
                .focused($iskeyboardShowing)
        }
        .contentShape(Rectangle())
        .onTapGesture {
            iskeyboardShowing.toggle()
        }
        .padding(.bottom,20)
        .padding(.top,10)
    }
    private var ResetCodeButton: some View {
        Button {
            
        } label: {
            ZStack{
                Image("resetCodeBox")
                    .resizable()
                    .frame(width: 80 ,height: 30)
                Text(LocalizedStringKey("resetCode"))
                    .foregroundColor(.blue)
                    .font(.system(size: 12))
            }
        }
    }
    private var ConfirmButton: some View {
        Button {
            print(otpText)
        } label: {
            ZStack {
                Image("AuthButton")
                    .resizable()
                    .frame(width: 100, height: 40)
                Text(LocalizedStringKey("confirm"))
                    .font(.custom("Cairo-Bold", size: 20))
                    .foregroundColor(.white)
                    .offset(y: -2.5)
            }
        }
    }
}
struct VerifiedCodeView_Previews: PreviewProvider {
    static var previews: some View {
        VerifiedCodeView()
    }
}

extension Binding where Value == String{
    func limit(_ length: Int , test : Binding<Bool>)->Self{
        if self.wrappedValue.count > length {
            DispatchQueue.main.async {
                test.wrappedValue = true
                self.wrappedValue = String(self.wrappedValue.prefix(length))
            }
        }
        return self
    }
}
