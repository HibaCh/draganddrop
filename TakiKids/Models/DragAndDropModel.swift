//
//  DragAndDropModel.swift
//  TakiKids
//
//  Created by hiba on 29/11/2022.
//

import Foundation

enum ModelState {
    case normal, correct, wrong, empty, selected
}

struct Model : Identifiable , Hashable , Equatable {
    var id: Int
    var image: String = ""
    var state: ModelState
    var linkedId : Int = -1
    var type: Views = .first
}

enum Views {
    case first, second , third
}



//enum ModelState {
//case normal, wrong, correct, empty, selected
//}
//
//struct Model: Identifiable, Hashable {
//let id: Int
//let image: String
//var linkedId: Int = -1
//var state: ModelState = .normal
//var isFlipped: Bool = false
//}
