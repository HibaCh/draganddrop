//
//  PlayGameQuestionsView.swift
//  TakiKids
//
//  Created by hiba on 12/1/2023.
//

import SwiftUI
import WebKit

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}

enum StateQuestion {
    case current, correct, wrong, closed
}

struct QuestionModel : Hashable{
    var id : Int
    var state : StateQuestion = .closed
}

var arrayQuestions  = [
    QuestionModel(id: 1 , state: .wrong),
    QuestionModel(id: 2 , state: .correct),
    QuestionModel(id: 3 , state: .current),
    QuestionModel(id: 4 ),
    QuestionModel(id: 5 ),
    QuestionModel(id: 6 ),
    QuestionModel(id: 7 ),
    QuestionModel(id: 8 ),
    QuestionModel(id: 9 ),
    QuestionModel(id: 10 ),
    QuestionModel(id: 11 ),
    QuestionModel(id: 12 ),
    QuestionModel(id: 13 ),
    QuestionModel(id: 14 ),
    QuestionModel(id: 15 ),
    QuestionModel(id: 16 ),
    QuestionModel(id: 17 ),
]






// Embed Youtube Video...
struct YoutubeVideoComponent : UIViewRepresentable {
    let videoId : String
    
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    
    func getYoutubeId(youtubeUrl: String) -> String? {
        return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        guard let youtubeURL = URL(string: "https://www.youtube.com/embed/\(videoId)") else { return }
        uiView.scrollView.isScrollEnabled = false
        uiView.load(URLRequest(url: youtubeURL))
    }
}
// Get Youtube Video ID...
func getYoutubeId(youtubeUrl: String) -> String? {
    return URLComponents(string: youtubeUrl)?.queryItems?.first(where: { $0.name == "v" })?.value
}
struct EducationalVideosView: View {
    let rows = [GridItem(.fixed(30))]
    @Binding var isShowing : Bool
    var body: some View {
        
        ZStack (alignment: .top){
            
            ZStack {
                ZStack{
                    HStack {
                        LazyHGrid(rows: rows) {
                            ForEach(0..<3) { index in
                                
                                YoutubeVideoComponent(videoId: "sIMc-px9oj4")
                                    .frame(width: (UIScreen.main.bounds.width * 0.7) / 3,height: 150)
                                    .cornerRadius(15)
                                    .padding(.horizontal, 5)
                            }
                        }
                        .fixedSize()
                    }
                }
            }
            .frame(width: UIScreen.main.bounds.width * 0.85, height: UIScreen.main.bounds.height * 0.6)
            .background(Color.white)
            .cornerRadius(25)
            
            HStack {
                ZStack{
                    
                    Text(LocalizedStringKey("English Lesson"))
                        .foregroundColor(.white)
                        .fontWeight(.heavy)
                        .padding(.leading, 80)
                }
                .offset(y:-50)
                Spacer()
                
                ZStack{
                    Button {
                        isShowing = false
                    } label: {
                        
                        Image(systemName: "xmark.circle.fill")
                            .resizable()
                            .frame(width: 40,height: 40)
                            .foregroundColor(.red)
                    }
                    
                    
                }
                .offset(x:15, y:-15)
            }
            .frame(width: UIScreen.main.bounds.width * 0.85)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.black.opacity(0.75))
        .ignoresSafeArea(.all)
        
    }
}


struct PlayGameQuestionsView: View {
    
    @State var isYout = false
    var width = UIScreen.main.bounds.width * 0.62
    var height = UIScreen.main.bounds.height * 0.75
    var body: some View {
        ZStack{
            // background
            Image("BG_Girl")
                .resizable()
                .ignoresSafeArea()

            HStack(){

                //vido button
                videoYoutubeButton

                //guestions box
                GameBoxQuestions()
                    .frame(width: width , height: height)
                //tesnim image
                Image("girli")
                    .resizable()
                    .frame(width: UIScreen.main.bounds.width * 0.19)
                //map and flag icons
                VStack{

                    Button {

                    } label: {
                        Image("position")
                            .resizable()
                            .frame(width: 50,height: 50)
                    }

                    Image("flag")
                        .resizable()
                        .frame(width: 50,height: 50)
                }
                .padding(.trailing)
                .padding(.top ,30)
                .frame(maxWidth:.infinity,maxHeight: .infinity,alignment: .topTrailing)
            }



            Image("Grouplap")
                .resizable()
                .frame(width: 80,height: 100)
                .padding(.bottom ,20)
                .frame(maxWidth:.infinity,maxHeight: .infinity,alignment: .bottomLeading)


                .frame(maxWidth:.infinity,maxHeight: .infinity,alignment: .topLeading)


            if isYout{
                EducationalVideosView(isShowing: $isYout)
            }
        }
        .onTapGesture {
            isYout = false
        }
    }
}
extension PlayGameQuestionsView {
    private var videoYoutubeButton: some View {
        
        ZStack{
            Button {
                isYout = true
            } label: {
                Image("Frame (1)")
                    .resizable()
                    .frame(width: 45,height: 45)
            }
        }
        .frame(maxWidth:.infinity,maxHeight: .infinity,alignment: .topLeading)
        .padding(.top ,30)
    }
}

struct GameBoxQuestions: View {
    @State private var selected = 0
    @State private var showWaves = false
    @State private var showWavesBackButton = false
    let result = arrayQuestions.chunked(into: 12)
    var body: some View {
        ZStack{
            //   background
            Image("boxPink")
                .resizable()
                .ignoresSafeArea()
            
            Image("Rectangle 27")
                .resizable()
                .frame(width: 200,height: 45)
                .overlay {
                    Text(LocalizedStringKey("levels"))
                        .foregroundColor(Color(red: 0.43137254901960786, green: 0.14901960784313725, blue: 0.403921568627451))
                }
                .offset(y:-140)
            
            HStack{
                Button {
                    withAnimation(.easeInOut) {
                        if selected > 0 {
                            selected  = selected - 1
                            print(selected)
                        }
                    }
                } label: {
                    Image("Vector (1)")
                        .resizable()
                        .rotationEffect(.degrees(180))
                        .frame(width: 15,height: 25)
                        .padding(10)
                }
                .scaleEffect(showWavesBackButton ? 1.2 : 1)
                .hueRotation(.degrees(showWavesBackButton ? 360 : 0))
                .opacity(showWavesBackButton ? 0.8 : 1)
                .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true).speed(3))
                .onAppear{
                    self.showWavesBackButton.toggle()
                }
                .opacity(selected != 0   ? 1 : 0)
                
                TabView(selection: $selected){
                    ForEach(result.indices) { indice in
                        Questions(questions: result[indice])
                            .tag(indice)
                    }
                }
                .tabViewStyle(.page(indexDisplayMode: .always))
                
                ZStack{
                    Button {
                        withAnimation(.easeInOut) {
                            if selected < result.count {
                                selected = selected + 1
                                print(selected)
                            }
                        }
                    } label: {
                        Image("Vector (1)")
                            .resizable()
                            .frame(width: 15,height: 25)
                        
                    }
                    .scaleEffect(showWaves ? 1.2 : 1)
                    .hueRotation(.degrees(showWaves ? 360 : 0))
                    .opacity(showWaves ? 0.8 : 1)
                    .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true).speed(3))
                    .onAppear{
                        self.showWaves.toggle()
                    }
                    .opacity(selected  != result.count-1 ? 1 : 0)
                }
                .padding(10)
            }
        }
    }
}
private func getState(state: StateQuestion) -> String? {
    switch state {
    case .current:
        return "Group (1)"
    case .correct:
        return "Vector (2)"
    case .wrong:
        return "Vector (3)"
    default:
        return "Frame"
    }
}
struct Questions: View {
    var questions : [QuestionModel]
    @State private var showWaves = false
    @State private var state = true
    //    var qustionNumber : Int
    var width = UIScreen.main.bounds.width * 0.60
    var height = UIScreen.main.bounds.height * 0.60
    let columns = [
        GridItem(.flexible(), alignment: .top),
        GridItem(.flexible(), alignment: .top),
        GridItem(.flexible(), alignment: .top),
        GridItem(.flexible(), alignment: .top)
    ]
    var body: some View {
        LazyVGrid(columns: columns) {
            ForEach(questions , id: \.self) {Qut in
                ZStack{
                    ZStack{
                        Image("Vector (4)")
                            .resizable()
                            .frame(width: 60,height: 60)
//
                        Image(getState(state: Qut.state) ?? "")
                            .resizable()
                            .frame(width: 55,height: 55)
//                            .resizable()
                    }
                 
                    Group{
                        Image("Ellipse 7")
                            .resizable()
                            .opacity(Qut.state != StateQuestion.current ? 0 : 1)
                            .frame(width: 40,height: 40)
                            .scaleEffect(showWaves ? 1.2 : 1)
                            .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true).speed(3))
                        Text("\(Qut.id)")
                            .bold()
                            .foregroundColor(.white)
                            .opacity(Qut.state == StateQuestion.closed ? 0 : 1)
                            .font(.system(size: 25))
                            .scaleEffect(Qut.state != StateQuestion.current ? 1 :showWaves ? 1.2 : 1)
                            .animation(Animation.easeInOut(duration: 1).repeatForever(autoreverses: true).speed(3))
                    }
                        .onAppear{
                            if Qut.state == StateQuestion.current{
                                self.showWaves.toggle()
                            }
                        }
                }
            }
        }
        .padding(.vertical)
        .padding(.horizontal,40)
        .frame(width: width , height: height, alignment: .topLeading)
    }
}

struct PlayGameQuestionsView_Previews: PreviewProvider {
    static var previews: some View {
        PlayGameQuestionsView()
        
    }
}
