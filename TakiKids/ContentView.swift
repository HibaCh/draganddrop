//
//  ContentView.swift
//  TakiKids
//
//  Created by hiba on 21/11/2022.
//

import SwiftUI

struct ContentView: View {
    
    @State var characters: [Character] = characters_
    //for Drag Part
    @State var shuffledRows: [[Character]] = []
    //for Drop Part
    @State var rows: [[Character]] = []
    
    var body: some View {
        VStack(){
            VStack {
                DropArea()
                    .padding(.vertical,30)
                DragArea()
            }
            .padding()
            .onAppear{
                if rows.isEmpty{
                    characters = characters.shuffled()
                    shuffledRows = generateGrid()
                    characters = characters_
                    rows = generateGrid()
                }
            }
        }
        
    }
    @ViewBuilder
    func DropArea()->some View{
        VStack(spacing:12) {
            ForEach($rows , id: \.self){$row in
                //print(row)
                HStack(spacing: 10){
                    ForEach($row){$item in
                        Text(item.value)
                            .font(.system(size: item.fontSize))
                            .padding(.vertical,5)
                            .padding(.horizontal,item.padding)
                            .opacity(item.isShowing ? 1 : 0)
                            .background{
                                RoundedRectangle(cornerRadius: 6, style: .continuous)
                                    .fill(item.isShowing ? .clear :
                                            .gray.opacity(0.25))
                            }
                            .background{
                                RoundedRectangle(cornerRadius: 6, style: .continuous)
                                    .stroke(.gray)
                            }
                            .onDrop(of: [.url], isTargeted: .constant(false)){
                                providers in
                                if let first =  providers.first{
                                    let _ = first.loadObject(ofClass: URL.self) { value, error in
                                        guard let url =  value else{return}
                                        print(url)
                                        if item.id == "\(url)"{
                                      
                                            withAnimation{
                                                item.isShowing = true
                                                updateShuffledArray(character: item)
                                            }
                                            
                                        }
                                    }
                                }
                                return false
                            }
                    }
                }
                if rows.last != row{
                    Divider()
                }
            }
        }
    }
    @ViewBuilder
    func DragArea()->some View{
        VStack(spacing:12) {
            ForEach(shuffledRows , id: \.self){row in
                HStack(spacing: 10){
                    ForEach(row){item in
                        
                        Text(item.value)
                       
                            .font(.system(size: item.fontSize))
                            .padding(.vertical,5)
                            .padding(.horizontal,item.padding)
                            .background{
                                RoundedRectangle(cornerRadius: 6, style: .continuous)
                                    .stroke(.gray)
                            }
                        
                            .opacity(item.isShowing ? 0 : 1)
                            .background{
                                RoundedRectangle(cornerRadius: 6, style: .continuous)
                                    .fill(item.isShowing ? .gray.opacity(0.25)  :
                                            .clear )
                            }
                        
                            .onDrag({
                              //  print(item.id)
                                return .init(contentsOf: URL(string: item.id))!
                            })
                        
                    }
                }
                if shuffledRows.last != row{
                    Divider()
                }
            }
        }
    }
    func generateGrid()->[[Character]]{
        for item in characters.enumerated(){
            let textSize = textSize(character : item.element)
            characters[item.offset].textSize = textSize
        }
        var gridArray : [[Character]] = []
        var tempArray : [Character] = []
        var currentWidth : CGFloat = 0
        let totalScreenWidth: CGFloat = UIScreen.main.bounds.width - 50
        
        for character in characters {
            currentWidth += character.textSize
            
            if currentWidth < totalScreenWidth{
                tempArray.append(character)
            }
            else{
                gridArray.append(tempArray)
                tempArray = []
                currentWidth = character.textSize
                tempArray.append(character)
            }
        }
        
        if !tempArray.isEmpty{
            gridArray.append(tempArray)
        }
        
        return gridArray
    }
    func textSize(character: Character)->CGFloat{
        let font = UIFont.systemFont(ofSize: character.fontSize)
        let attributes = [NSAttributedString.Key.font : font]
        let size = (character.value as NSString).size(withAttributes: attributes)
        return size.width + (character.padding * 2) + 15
    }
    func updateShuffledArray(character : Character){
        for index in shuffledRows.indices{
            for subIndex in shuffledRows[index].indices{
                if shuffledRows[index][subIndex].id == character.id{
                    shuffledRows[index][subIndex].isShowing = true
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
