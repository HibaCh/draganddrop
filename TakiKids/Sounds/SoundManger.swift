//
//  SoundManger.swift
//  TakiKids
//
//  Created by hiba on 7/12/2022.
//

import Foundation
import AVKit
class SoundManger{
    static let instance = SoundManger()
    var player : AVAudioPlayer?
    enum SoundOption: String {
        case tada
        case badum
    }
    func playSound(sound: SoundOption){
        guard let url = Bundle.main.url(forResource: sound.rawValue, withExtension: ".mp3")else{return}
        do{
            player = try AVAudioPlayer(contentsOf: url)
            player?.play()
        }catch let err {
            print("error playing sound",err.localizedDescription)
        }
    }
}
