//
//  test2.swift
//  TakiKids
//
//  Created by hiba on 22/11/2022.
//

//
//import SwiftUI
//

//
//
//
//
//
//
//enum ModelState {
//    case normal, correct, wrong, empty
//}
//
//
//struct dragDrop : Identifiable , Hashable , Equatable {
//    let id: String
//    var value: String = ""
//    var color: Color = .gray
//    var state: ModelState
//    var linkedId : String = ""
//}
//
//struct test2: View {
//
// //   @State var itemDraggged: String = ""
//
//    @State var bottomItems: [dragDrop] = [
//        dragDrop(id: "1", value:"my", state: ModelState.normal , linkedId: "1"),
//        dragDrop(id: "2",value:"name", state: ModelState.normal, linkedId: "2"),
//        dragDrop(id: "3",value:"is", state: ModelState.normal, linkedId: "3"),
//        dragDrop(id: "4",value:"hiba", state: ModelState.normal, linkedId: "4")
//    ].shuffled()
//
//    @State var topItems: [dragDrop] = [
//        dragDrop(id: "5",value:"", state: ModelState.empty , linkedId: "5"),
//        dragDrop(id: "6",value:"" , state: ModelState.empty, linkedId: "6"),
//        dragDrop(id: "7",value:"", state: ModelState.empty, linkedId: "7"),
//        dragDrop(id: "8",value:"",  state: ModelState.empty, linkedId: "8")
//    ]
//    var correctList: [[String]] = [["1", "5"], ["2", "6"], ["3", "7"], ["4", "8"]]
//
//    var body: some View {
//        VStack(){
//            VStack {
//                HStack{
//                    ForEach (topItems , id: \.self) { item in
//                        DropArea(content:View2(item: item), item: item)
//                    }
//                }
//                .padding(.vertical,30)
//                HStack{
//                    ForEach (bottomItems , id: \.self) { item in
//                        DragArea(content: View1(item: item), item: item)
//                    }
//                }
//                Spacer()
//                Button {
//                    test()
//
//                } label: {
//                    Image(systemName: "checkmark.diamond.fill")
//                        .font(.system(size: 30))
//                }
//            }
//            .padding()
//        }
//    }
//
//    func test() {
//        for index in topItems.indices {
//            let item = topItems[index]
//            print([item.id, item.linkedId])
//            topItems[index].state = ModelState.wrong
//            topItems[index].color = .red
//            if correctList.contains([item.id, item.linkedId]) || correctList.contains([item.linkedId, item.id]) {
//                topItems[index].state = ModelState.correct
//                topItems[index].color = .green
//            }
//        }
//    }
//
//
//    @ViewBuilder
//    func DragArea( content : some View , item : dragDrop)->some View {
//        HStack(spacing:12) {
//                content
//                .frame(width: 80 ,height: 80)
//                .onDrop(of: [.url], isTargeted: .constant(false)){ providers in
//                    if let first =  providers.first{
//                        let _ = first.loadObject(ofClass: URL.self) { value, error in
//                            guard let url =  value else{return}
//                            dropFunc(FirstItems: $topItems, SecondItems: $bottomItems, item: item, url:"\(url)")
//                        }
//                    }
//                    return false
//                }
//                .drag(if: item.value != "") {
//                    return .init(contentsOf: URL(string: item.id))!
//                }
//                .onTapGesture(count: 2) {
//                    if let  emptyIndex = topItems.firstIndex( where: { $0.value == "" } ) {
//                        let aux =  topItems[emptyIndex].value
//                        let currentItemIndex = bottomItems.firstIndex(where: {$0.id == item.id})!
//                        topItems[emptyIndex].value = bottomItems[currentItemIndex].value
//                        bottomItems[currentItemIndex].value = aux
//
//                        topItems[emptyIndex].color = .gray
//                        bottomItems[currentItemIndex].color = .gray
//
//                        let auxState =  topItems[emptyIndex].state
//                        let StateCurrentItemIndex = bottomItems.firstIndex(where: {$0.id == item.id})!
//                        topItems[emptyIndex].state = bottomItems[StateCurrentItemIndex].state
//                        bottomItems[currentItemIndex].state = auxState
//
//                        let auxLinkedId =  topItems[emptyIndex].linkedId
//                        let LinkedIdCurrentItemIndex = bottomItems.firstIndex(where: {$0.id == item.id})!
//                        topItems[emptyIndex].linkedId = bottomItems[LinkedIdCurrentItemIndex].linkedId
//                        bottomItems[currentItemIndex].linkedId = auxLinkedId
//                    }
//                }
//        }
//    }
//
//
//    @ViewBuilder
//    func DropArea( content : some View , item : dragDrop)->some View {
//        HStack(spacing:12) {
//            content
//                .frame(width: 80 ,height: 80)
//                .onDrop(of: [.url], isTargeted: .constant(false)){ providers in
//                    //itemDraggged = ""
//
//                    if let first =  providers.first{
//                        let _ = first.loadObject(ofClass: URL.self) { value, error in
//                            guard let url =  value else{return}
//                            // print(url)
//
//                            dropFunc(FirstItems: $bottomItems, SecondItems: $topItems, item: item, url:"\(url)")
//
//                            dropFunc(FirstItems: $topItems, SecondItems: $topItems, item: item, url:"\(url)")
//
//                        }
//                    }
//                    return false
//                }
//                .drag(if: item.value != "") {
//                    return .init(contentsOf: URL(string: item.id))!
//                }
//                .onTapGesture(count: 2) {
//                    if let  emptyIndex = bottomItems.firstIndex( where: { $0.value == "" } ) {
//                        let aux =  bottomItems[emptyIndex].value
//                        let currentItemIndex = topItems.firstIndex(where: {$0.id == item.id})!
//                        bottomItems[emptyIndex].value = topItems[currentItemIndex].value
//                        topItems[currentItemIndex].value = aux
//
//
//                        bottomItems[emptyIndex].color = .gray
//                        topItems[currentItemIndex].color = .gray
//
//                        let auxState =  bottomItems[emptyIndex].state
//                        let stateCurrentItemIndex = topItems.firstIndex(where: {$0.id == item.id})!
//                        bottomItems[emptyIndex].state = topItems[stateCurrentItemIndex].state
//                        topItems[currentItemIndex].state = auxState
//
//                        let auxLinkedId =  bottomItems[emptyIndex].linkedId
//                        let LinkedIdCurrentItemIndex = topItems.firstIndex(where: {$0.id == item.id})!
//                        bottomItems[emptyIndex].linkedId = topItems[LinkedIdCurrentItemIndex].linkedId
//                        topItems[currentItemIndex].linkedId = auxLinkedId
//
//
//                    }
//                }
//        }
//    }
//
//
//    func dropFunc(FirstItems : Binding<[dragDrop]> ,SecondItems: Binding<[dragDrop]>,  item :  dragDrop , url : String  ){
//        if let TopToBottomIndex = FirstItems.firstIndex( where: { $0.id == "\(url)" } ) {
//            let aux = item.value
//            let currentItemIndex = SecondItems.firstIndex(where: {$0.id == item.id})!
//            SecondItems[currentItemIndex].value.wrappedValue = FirstItems[TopToBottomIndex].value.wrappedValue
//            FirstItems[TopToBottomIndex].value.wrappedValue = aux
//
//
//            let currentItemIndex2 = SecondItems.firstIndex(where: {$0.id == item.id})!
//            SecondItems[currentItemIndex2].color.wrappedValue = .gray
//            FirstItems[TopToBottomIndex].color.wrappedValue = .gray
//
//            let aux3 = item.state
//            let currentItemIndex3 = SecondItems.firstIndex(where: {$0.id == item.id})!
//            SecondItems[currentItemIndex3].state.wrappedValue = FirstItems[TopToBottomIndex].state.wrappedValue
//            FirstItems[TopToBottomIndex].state.wrappedValue = aux3
//
//            let aux4 = item.linkedId
//            let currentItemIndex4 = SecondItems.firstIndex(where: {$0.id == item.id})!
//            SecondItems[currentItemIndex4].linkedId.wrappedValue = FirstItems[TopToBottomIndex].linkedId.wrappedValue
//            FirstItems[TopToBottomIndex].linkedId.wrappedValue = aux4
//
//
//            print(SecondItems[currentItemIndex4])
//            print(FirstItems[TopToBottomIndex])
//
//        }
//    }
//
//
//}
