//
//  TakiKidsApp.swift
//  TakiKids
//
//  Created by hiba on 21/11/2022.
//

import SwiftUI

@main
struct TakiKidsApp: App {
    var body: some Scene {
        WindowGroup {
            PlayGameQuestionsView()
        }
    }
}
