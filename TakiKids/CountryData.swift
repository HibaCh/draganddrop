//
//  CountryData.swift
//  EnglishApp
//
//  Created by hiba on 19/7/2022.
//

import Foundation
struct Country: Identifiable{
    var id = UUID()
    var code:String
    var name: String
    var dialCode: String
  
}
let CountryData: [Country] = [
    Country(
        code: "AD",
        name: "أندورا",
        dialCode: "+376"
    ),
    Country(
        code: "AE",
        name: "الامارات العربية المتحدة",
        dialCode: "+971"
    ),
    Country(
        code: "AF",
        name: "أفغانستان",
        dialCode: "+93"
    ),
    Country(
        code: "AG",
        name: "أنتيجوا وبربودا",
        dialCode: "+1"
    ),
    Country(
        code: "AI",
        name: "أنجويلا",
        dialCode: "+1"
    ),
    Country(
        code: "AL",
        name: "ألبانيا",
        dialCode: "+355"
    ),
    Country(
        code: "AM",
        name: "أرمينيا",
        dialCode: "+374"
    ),
    Country(
        code: "AO",
        name: "أنجولا",
        dialCode: "+244"
    ),
    Country(
        code: "AQ",
        name: "القطب الجنوبي",
        dialCode: "+672"
    ),
    Country(
        code: "AR",
        name: "الأرجنتين",
        dialCode: "+54"
    ),
    Country(
        code: "AS",
        name: "ساموا الأمريكية",
        dialCode: "+1"
    ),
    Country(
        code: "AT",
        name: "النمسا",
        dialCode: "+43"
    ),
    Country(
        code: "AU",
        name: "أستراليا",
        dialCode: "+61"
    ),
    Country(
        code: "AW",
        name: "آروبا",
        dialCode: "+297"
    ),
    Country(
        code: "AX",
        name: "جزر أولان",
        dialCode: "+358"
    ), 
    Country(
        code: "AZ",
        name: "أذربيجان",
        dialCode: "+994"
    ),
    Country(
        code: "BA",
        name: "البوسنة والهرسك",
        dialCode: "+387"
    ),
    Country(
        code: "BB",
        name: "بربادوس",
        dialCode: "+1"
    ),
    Country(
        code: "BD",
        name: "بنجلاديش",
        dialCode: "+880"
    ),
    Country(
        code: "BE",
        name: "بلجيكا",
        dialCode: "+32"
    ),
    Country(
        code: "BF",
        name: "بوركينا فاسو",
        dialCode: "+226"
    ),
    Country(
        code: "BG",
        name: "بلغاريا",
        dialCode: "+359"
    ),
    Country(
        code: "BH",
        name: "البحرين",
        dialCode: "+973"
    ),
    Country(
        code: "BI",
        name: "بوروندي",
        dialCode: "+257"
    ),
    Country(
        code: "BJ",
        name: "بنين",
        dialCode: "+229"
    ),
    Country(
        code: "BL",
        name: "سان بارتيلمي",
        dialCode: "+590"
    ),
    Country(
        code: "BM",
        name: "برمودا",
        dialCode: "+1"
    ),
    Country(
        code: "BN",
        name: "بروناي",
        dialCode: "+673"
    ),
    Country(
        code: "BO",
        name: "بوليفيا",
        dialCode: "+591"
    ),
    Country(
        code: "BQ",
        name: "بونير",
        dialCode: "+599"
    ),
    Country(
        code: "BR",
        name: "البرازيل",
        dialCode: "+55"
    ),
    Country(
        code: "BS",
        name: "الباهاما",
        dialCode: "+1"
    ),
    Country(
        code: "BT",
        name: "بوتان",
        dialCode: "+975"
    ),
    Country(
        code: "BV",
        name: "جزيرة بوفيه",
        dialCode: "+47"
    ),
    Country(
        code: "BW",
        name: "بتسوانا",
        dialCode: "+267"
    ),
    Country(
        code: "BY",
        name: "روسيا البيضاء",
        dialCode: "+375"
    ),
    Country(
        code: "BZ",
        name: "بليز",
        dialCode: "+501"
    ),
    Country(
        code: "CA",
        name: "كندا",
        dialCode: "+1"
    ),
    Country(
        code: "CC",
        name: "جزر كوكوس",
        dialCode: "+61"
    ),
    Country(
        code: "CD",
        name: "جمهورية الكونغو الديمقراطية",
        dialCode: "+243"
    ),
    Country(
        code: "CF",
        name: "جمهورية افريقيا الوسطى",
        dialCode: "+236"
    ),
    Country(
        code: "CG",
        name: "الكونغو - برازافيل",
        dialCode: "+242"
    ),
    Country(
        code: "CH",
        name: "سويسرا",
        dialCode: "+41"
    ),
    Country(
        code: "CI",
        name: "ساحل العاج",
        dialCode: "+225"
    ),
    Country(
        code: "CK",
        name: "جزر كوك",
        dialCode: "+682"
    ),
    Country(
        code: "CL",
        name: "شيلي",
        dialCode: "+56"
    ),
    Country(
        code: "CM",
        name: "الكاميرون",
        dialCode: "+237"
    ),
    Country(
        code: "CN",
        name: "الصين",
        dialCode: "+86"
    ),
    Country(
        code: "CO",
        name: "كولومبيا",
        dialCode: "+57"
    ),
    Country(
        code: "CR",
        name: "كوستاريكا",
        dialCode: "+506"
    ),
    Country(
        code: "CU",
        name: "كوبا",
        dialCode: "+53"
    ),
    Country(
        code: "CV",
        name: "الرأس الأخضر",
        dialCode: "+238"
    ),
    Country(
        code: "CW",
        name: "كوراساو",
        dialCode: "+599"
    ),
    Country(
        code: "CX",
        name: "جزيرة الكريسماس",
        dialCode: "+61"
    ),
    Country(
        code: "CY",
        name: "قبرص",
        dialCode: "+357"
    ),
    Country(
        code: "CZ",
        name: "جمهورية التشيك",
        dialCode: "+420"
    ),
    Country(
        code: "DE",
        name: "ألمانيا",
        dialCode: "+49"
    ),
    Country(
        code: "DJ",
        name: "جيبوتي",
        dialCode: "+253"
    ),
    Country(
        code: "DK",
        name: "الدانمرك",
        dialCode: "+45"
    ),
    Country(
        code: "DM",
        name: "دومينيكا",
        dialCode: "+1"
    ),
    Country(
        code: "DO",
        name: "جمهورية الدومينيك",
        dialCode: "+1"
    ),
    Country(
        code: "DZ",
        name: "الجزائر",
        dialCode: "+213"
    ),
    Country(
        code: "EC",
        name: "الاكوادور",
        dialCode: "+593"
    ),
    Country(
        code: "EE",
        name: "استونيا",
        dialCode: "+372"
    ),
    Country(
        code: "EG",
        name: "مصر",
        dialCode: "+20"
    ),
    Country(
        code: "EH",
        name: "الصحراء الغربية",
        dialCode: "+212"
    ),
    Country(
        code: "ER",
        name: "اريتريا",
        dialCode: "+291"
    ),
    Country(
        code: "ES",
        name: "أسبانيا",
        dialCode: "+34"
    ),
    Country(
        code: "ET",
        name: "اثيوبيا",
        dialCode: "+251"
    ),
    Country(
        code: "FI",
        name: "فنلندا",
        dialCode: "+358"
    ),
    Country(
        code: "FJ",
        name: "فيجي",
        dialCode: "+679"
    ),
    Country(
        code: "FK",
        name: "جزر فوكلاند",
        dialCode: "+500"
    ),
    Country(
        code: "FM",
        name: "ميكرونيزيا",
        dialCode: "+691"
    ),
    Country(
        code: "FO",
        name: "جزر فارو",
        dialCode: "+298"
    ),
    Country(
        code: "FR",
        name: "فرنسا",
        dialCode: "+33"
    ),
    Country(
        code: "GA",
        name: "الجابون",
        dialCode: "+241"
    ),
    Country(
        code: "GB",
        name: "المملكة المتحدة",
        dialCode: "+44"
    ),
    Country(
        code: "GD",
        name: "جرينادا",
        dialCode: "+1"
    ),
    Country(
        code: "GE",
        name: "جورجيا",
        dialCode: "+995"
    ),
    Country(
        code: "GF",
        name: "غويانا",
        dialCode: "+594"
    ),
    Country(
        code: "GG",
        name: "غيرنزي",
        dialCode: "+44"
    ),
    Country(
        code: "GH",
        name: "غانا",
        dialCode: "+233"
    ),
    Country(
        code: "GI",
        name: "جبل طارق",
        dialCode: "+350"
    ),
    Country(
        code: "GL",
        name: "جرينلاند",
        dialCode: "+299"
    ),
    Country(
        code: "GM",
        name: "غامبيا",
        dialCode: "+220"
    ),
    Country(
        code: "GN",
        name: "غينيا",
        dialCode: "+224"
    ),
    Country(
        code: "GP",
        name: "جوادلوب",
        dialCode: "+590"
    ),
    Country(
        code: "GQ",
        name: "غينيا الاستوائية",
        dialCode: "+240"
    ),
    Country(
        code: "GR",
        name: "اليونان",
        dialCode: "+30"
    ),
    Country(
        code: "GS",
        name: "جورجيا الجنوبية وجزر ساندويتش الجنوبية",
        dialCode: "+500"
    ),
    Country(
        code: "GT",
        name: "جواتيمالا",
        dialCode: "+502"
    ),
    Country(
        code: "GU",
        name: "جوام",
        dialCode: "+1"
    ),
    Country(
        code: "GW",
        name: "غينيا بيساو",
        dialCode: "+245"
    ),
    Country(
        code: "GY",
        name: "غيانا",
        dialCode: "+595"
    ),
    Country(
        code: "HK",
        name: "هونج كونج الصينية",
        dialCode: "+852"
    ),
    Country(
        code: "HM",
        name: "جزيرة هيرد وماكدونالد",
        dialCode: ""
    ),
    Country(
        code: "HN",
        name: "هندوراس",
        dialCode: "+504"
    ),
    Country(
        code: "HR",
        name: "كرواتيا",
        dialCode: "+385"
    ),
    Country(
        code: "HT",
        name: "هايتي",
        dialCode: "+509"
    ),
    Country(
        code: "HU",
        name: "المجر",
        dialCode: "+36"
    ),
    Country(
        code: "ID",
        name: "اندونيسيا",
        dialCode: "+62"
    ),
    Country(
        code: "IE",
        name: "أيرلندا",
        dialCode: "+353"
    ),
    Country(
        code: "IM",
        name: "جزيرة مان",
        dialCode: "+44"
    ),
    Country(
        code: "IN",
        name: "الهند",
        dialCode: "+91"
    ),
    Country(
        code: "IO",
        name: "المحيط الهندي البريطاني",
        dialCode: "+246"
    ),
    Country(
        code: "IQ",
        name: "العراق",
        dialCode: "+964"
    ),
    Country(
        code: "IR",
        name: "ايران",
        dialCode: "+98"
    ),
    Country(
        code: "IS",
        name: "أيسلندا",
        dialCode: "+354"
    ),
    Country(
        code: "IT",
        name: "ايطاليا",
        dialCode: "+39"
    ),
    Country(
        code: "JE",
        name: "جيرسي",
        dialCode: "+44"
    ),
    Country(
        code: "JM",
        name: "جامايكا",
        dialCode: "+1"
    ),
    Country(
        code: "JO",
        name: "الأردن",
        dialCode: "+962"
    ),
    Country(
        code: "JP",
        name: "اليابان",
        dialCode: "+81"
    ),
    Country(
        code: "KE",
        name: "كينيا",
        dialCode: "+254"
    ),
    Country(
        code: "KG",
        name: "قرغيزستان",
        dialCode: "+996"
    ),
    Country(
        code: "KH",
        name: "كمبوديا",
        dialCode: "+855"
    ),
    Country(
        code: "KI",
        name: "كيريباتي",
        dialCode: "+686"
    ),
    Country(
        code: "KM",
        name: "جزر القمر",
        dialCode: "+269"
    ),
    Country(
        code: "KN",
        name: "سانت كيتس ونيفيس",
        dialCode: "+1"
    ),
    Country(
        code: "KP",
        name: "كوريا الشمالية",
        dialCode: "+850"
    ),
    Country(
        code: "KR",
        name: "كوريا الجنوبية",
        dialCode: "+82"
    ),
    Country(
        code: "KW",
        name: "الكويت",
        dialCode: "+965"
    ),
    Country(
        code: "KY",
        name: "جزر الكايمن",
        dialCode: "+345"
    ),
    Country(
        code: "KZ",
        name: "كازاخستان",
        dialCode: "+7"
    ),
    Country(
        code: "LA",
        name: "لاوس",
        dialCode: "+856"
    ),
    Country(
        code: "LB",
        name: "لبنان",
        dialCode: "+961"
    ),
    Country(
        code: "LC",
        name: "سانت لوسيا",
        dialCode: "+1"
    ),
    Country(
        code: "LI",
        name: "ليختنشتاين",
        dialCode: "+423"
    ),
    Country(
        code: "LK",
        name: "سريلانكا",
        dialCode: "+94"
    ),
    Country(
        code: "LR",
        name: "ليبيريا",
        dialCode: "+231"
    ),
    Country(
        code: "LS",
        name: "ليسوتو",
        dialCode: "+266"
    ),
    Country(
        code: "LT",
        name: "ليتوانيا",
        dialCode: "+370"
    ),
    Country(
        code: "LU",
        name: "لوكسمبورج",
        dialCode: "+352"
    ),
    Country(
        code: "LV",
        name: "لاتفيا",
        dialCode: "+371"
    ),
    Country(
        code: "LY",
        name: "ليبيا",
        dialCode: "+218"
    ),
    Country(
        code: "MA",
        name: "المغرب",
        dialCode: "+212"
    ),
    Country(
        code: "MC",
        name: "موناكو",
        dialCode: "+377"
    ),
    Country(
        code: "MD",
        name: "مولدافيا",
        dialCode: "+373"
    ),
    Country(
        code: "ME",
        name: "الجبل الأسود",
        dialCode: "+382"
    ),
    Country(
        code: "MF",
        name: "سانت مارتين",
        dialCode: "+590"
    ),
    Country(
        code: "MG",
        name: "مدغشقر",
        dialCode: "+261"
    ),
    Country(
        code: "MH",
        name: "جزر المارشال",
        dialCode: "+692"
    ),
    Country(
        code: "MK",
        name: "مقدونيا",
        dialCode: "+389"
    ),
    Country(
        code: "ML",
        name: "مالي",
        dialCode: "+223"
    ),
    Country(
        code: "MM",
        name: "ميانمار",
        dialCode: "+95"
    ),
    Country(
        code: "MN",
        name: "منغوليا",
        dialCode: "+976"
    ),
    Country(
        code: "MO",
        name: "ماكاو الصينية",
        dialCode: "+853"
    ),
    Country(
        code: "MP",
        name: "جزر ماريانا الشمالية",
        dialCode: "+1"
    ),
    Country(
        code: "MQ",
        name: "مارتينيك",
        dialCode: "+596"
    ),
    Country(
        code: "MR",
        name: "موريتانيا",
        dialCode: "+222"
    ),
    Country(
        code: "MS",
        name: "مونتسرات",
        dialCode: "+1"
    ),
    Country(
        code: "MT",
        name: "مالطا",
        dialCode: "+356"
    ),
    Country(
        code: "MU",
        name: "موريشيوس",
        dialCode: "+230"
    ),
    Country(
        code: "MV",
        name: "جزر الملديف",
        dialCode: "+960"
    ),
    Country(
        code: "MW",
        name: "ملاوي",
        dialCode: "+265"
    ),
    Country(
        code: "MX",
        name: "المكسيك",
        dialCode: "+52"
    ),
    Country(
        code: "MY",
        name: "ماليزيا",
        dialCode: "+60"
    ),
    Country(
        code: "MZ",
        name: "موزمبيق",
        dialCode: "+258"
    ),
    Country(
        code: "NA",
        name: "ناميبيا",
        dialCode: "+264"
    ),
    Country(
        code: "NC",
        name: "كاليدونيا الجديدة",
        dialCode: "+687"
    ),
    Country(
        code: "NE",
        name: "النيجر",
        dialCode: "+227"
    ),
    Country(
        code: "NF",
        name: "جزيرة نورفوك",
        dialCode: "+672"
    ),
    Country(
        code: "NG",
        name: "نيجيريا",
        dialCode: "+234"
    ),
    Country(
        code: "NI",
        name: "نيكاراجوا",
        dialCode: "+505"
    ),
    Country(
        code: "NL",
        name: "هولندا",
        dialCode: "+31"
    ),
    Country(
        code: "NO",
        name: "النرويج",
        dialCode: "+47"
    ),
    Country(
        code: "NP",
        name: "نيبال",
        dialCode: "+977"
    ),
    Country(
        code: "NR",
        name: "نورو",
        dialCode: "+674"
    ),
    Country(
        code: "NU",
        name: "نيوي",
        dialCode: "+683"
    ),
    Country(
        code: "NZ",
        name: "نيوزيلاندا",
        dialCode: "+64"
    ),
    Country(
        code: "OM",
        name: "عمان",
        dialCode: "+968"
    ),
    Country(
        code: "PA",
        name: "بنما",
        dialCode: "+507"
    ),
    Country(
        code: "PE",
        name: "بيرو",
        dialCode: "+51"
    ),
    Country(
        code: "PF",
        name: "بولينيزيا الفرنسية",
        dialCode: "+689"
    ),
    Country(
        code: "PG",
        name: "بابوا غينيا الجديدة",
        dialCode: "+675"
    ),
    Country(
        code: "PH",
        name: "الفيلبين",
        dialCode: "+63"
    ),
    Country(
        code: "PK",
        name: "باكستان",
        dialCode: "+92"
    ),
    Country(
        code: "PL",
        name: "بولندا",
        dialCode: "+48"
    ),
    Country(
        code: "PM",
        name: "سانت بيير وميكولون",
        dialCode: "+508"
    ),
    Country(
        code: "PN",
        name: "بتكايرن",
        dialCode: "+872"
    ),
    Country(
        code: "PR",
        name: "بورتوريكو",
        dialCode: "+1"
    ),
    Country(
        code: "PS",
        name: "فلسطين",
        dialCode: "+970"
    ),
    Country(
        code: "PT",
        name: "البرتغال",
        dialCode: "+351"
    ),
    Country(
        code: "PW",
        name: "بالاو",
        dialCode: "+680"
    ),
    Country(
        code: "PY",
        name: "باراجواي",
        dialCode: "+595"
    ),
    Country(
        code: "QA",
        name: "قطر",
        dialCode: "+974"
    ),
    Country(
        code: "RE",
        name: "روينيون",
        dialCode: "+262"
    ),
    Country(
        code: "RO",
        name: "رومانيا",
        dialCode: "+40"
    ),
    Country(
        code: "RS",
        name: "صربيا",
        dialCode: "+381"
    ),
    Country(
        code: "RU",
        name: "روسيا",
        dialCode: "+7"
    ),
    Country(
        code: "RW",
        name: "رواندا",
        dialCode: "+250"
    ),
    Country(
        code: "SA",
        name: "المملكة العربية السعودية",
        dialCode: "+966"
    ),
    Country(
        code: "SB",
        name: "جزر سليمان",
        dialCode: "+677"
    ),
    Country(
        code: "SC",
        name: "سيشل",
        dialCode: "+248"
    ),
    Country(
        code: "SD",
        name: "السودان",
        dialCode: "+249"
    ),
    Country(
        code: "SE",
        name: "السويد",
        dialCode: "+46"
    ),
    Country(
        code: "SG",
        name: "سنغافورة",
        dialCode: "+65"
    ),
    Country(
        code: "SH",
        name: "سانت هيلنا",
        dialCode: "+290"
    ),
    Country(
        code: "SI",
        name: "سلوفينيا",
        dialCode: "+386"
    ),
    Country(
        code: "SJ",
        name: "سفالبارد وجان مايان",
        dialCode: "+47"
    ),
    Country(
        code: "SK",
        name: "سلوفاكيا",
        dialCode: "+421"
    ),
    Country(
        code: "SL",
        name: "سيراليون",
        dialCode: "+232"
    ),
    Country(
        code: "SM",
        name: "سان مارينو",
        dialCode: "+378"
    ),
    Country(
        code: "SN",
        name: "السنغال",
        dialCode: "+221"
    ),
    Country(
        code: "SO",
        name: "الصومال",
        dialCode: "+252"
    ),
    Country(
        code: "SR",
        name: "سورينام",
        dialCode: "+597"
    ),
    Country(
        code: "SS",
        name: "جنوب السودان",
        dialCode: "+211"
    ),
    Country(
        code: "ST",
        name: "ساو تومي وبرينسيبي",
        dialCode: "+239"
    ),
    Country(
        code: "SV",
        name: "السلفادور",
        dialCode: "+503"
    ),
    Country(
        code: "SX",
        name: "سينت مارتن",
        dialCode: "+1"
    ),
    Country(
        code: "SY",
        name: "سوريا",
        dialCode: "+963"
    ),
    Country(
        code: "SZ",
        name: "سوازيلاند",
        dialCode: "+268"
    ),
    Country(
        code: "TC",
        name: "جزر الترك وجايكوس",
        dialCode: "+1"
    ),
    Country(
        code: "TD",
        name: "تشاد",
        dialCode: "+235"
    ),
    Country(
        code: "TF",
        name: "المقاطعات الجنوبية الفرنسية",
        dialCode: "+262"
    ),
    Country(
        code: "TG",
        name: "توجو",
        dialCode: "+228"
    ),
    Country(
        code: "TH",
        name: "تايلند",
        dialCode: "+66"
    ),
    Country(
        code: "TJ",
        name: "طاجكستان",
        dialCode: "+992"
    ),
    Country(
        code: "TK",
        name: "توكيلو",
        dialCode: "+690"
    ),
    Country(
        code: "TL",
        name: "تيمور الشرقية",
        dialCode: "+670"
    ),
    Country(
        code: "TM",
        name: "تركمانستان",
        dialCode: "+993"
    ),
    Country(
        code: "TN",
        name: "تونس",
        dialCode: "+216"
    ),
    Country(
        code: "TO",
        name: "تونجا",
        dialCode: "+676"
    ),
    Country(
        code: "TR",
        name: "تركيا",
        dialCode: "+90"
    ),
    Country(
        code: "TT",
        name: "ترينيداد وتوباغو",
        dialCode: "+1"
    ),
    Country(
        code: "TV",
        name: "توفالو",
        dialCode: "+688"
    ),
    Country(
        code: "TW",
        name: "تايوان",
        dialCode: "+886"
    ),
    Country(
        code: "TZ",
        name: "تانزانيا",
        dialCode: "+255"
    ),
    Country(
        code: "UA",
        name: "أوكرانيا",
        dialCode: "+380"
    ),
    Country(
        code: "UG",
        name: "أوغندا",
        dialCode: "+256"
    ),
    Country(
        code: "UM",
        name: "جزر الولايات المتحدة البعيدة الصغيرة",
        dialCode: ""
    ),
    Country(
        code: "US",
        name: "الولايات المتحدة الأمريكية",
        dialCode: "+1"
    ),
    Country(
        code: "UY",
        name: "أورجواي",
        dialCode: "+598"
    ),
    Country(
        code: "UZ",
        name: "أوزبكستان",
        dialCode: "+998"
    ),
    Country(
        code: "VA",
        name: "الفاتيكان",
        dialCode: "+379"
    ),
    Country(
        code: "VC",
        name: "سانت فنسنت وغرنادين",
        dialCode: "+1"
    ),
    Country(
        code: "VE",
        name: "فنزويلا",
        dialCode: "+58"
    ),
    Country(
        code: "VG",
        name: "جزر فرجين البريطانية",
        dialCode: "+1"
    ),
    Country(
        code: "VI",
        name: "جزر فرجين الأمريكية",
        dialCode: "+1"
    ),
    Country(
        code: "VN",
        name: "فيتنام",
        dialCode: "+84"
    ),
    Country(
        code: "VU",
        name: "فانواتو",
        dialCode: "+678"
    ),
    Country(
        code: "WF",
        name: "جزر والس وفوتونا",
        dialCode: "+681"
    ),
    Country(
        code: "WS",
        name: "ساموا",
        dialCode: "+685"
    ),
    Country(
        code: "XK",
        name: "كوسوفو",
        dialCode: "+383"
    ),
    Country(
        code: "YE",
        name: "اليمن",
        dialCode: "+967"
    ),
    Country(
        code: "YT",
        name: "مايوت",
        dialCode: "+262"
    ),
    Country(
        code: "ZA",
        name: "جمهورية جنوب افريقيا",
        dialCode: "+27"
    ),
    Country(
        code: "ZM",
        name: "زامبيا",
        dialCode: "+260"
    ),
    Country(
        code: "ZW",
        name: "زيمبابوي",
        dialCode: "+263"
    )
]
