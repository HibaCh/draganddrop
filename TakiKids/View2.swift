//
//  View2.swift
//  TakiKids
//
//  Created by hiba on 24/11/2022.
//

import SwiftUI

struct View2: View {
    @State var item: dragDrop
    var body: some View {
        ZStack{
            Circle()
                .foregroundColor(item.state == ModelState.empty ? item.color.opacity(0.4) : item.color)
            Text(item.state == ModelState.empty ? "" : item.value)
        }
    }
}

