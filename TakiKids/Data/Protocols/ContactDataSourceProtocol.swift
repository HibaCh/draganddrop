import Foundation
protocol ContactDataSourceProtocol{
    func register(_ contactRequestModel: RegisterModel) async -> Result<Bool, ContactError>
}
